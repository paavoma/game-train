package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;

import model.GameTrain;
import model.PasswordHash;
import model.User;

/**
 * This class is used to test passwordhas class methods.
 * @author Eelis Melto
 */
class PasswordHashClassTest {
	

	private PasswordHash passwordhash = new PasswordHash();
	private static GameTrain db = new GameTrain();
	private User user;
	private static String email = "test@test.com";
	private static String newemail = "newtest@newtest.com";
	private String password = "test";
	private String wrongpassword = "wrong";
	
	@AfterAll
	static void deleteTestUsers() {
		db.deleteUser(db.readUserByEmail(newemail).getUserID());
		db.deleteUser(db.readUserByEmail(email).getUserID());
	}
	/**
	 * Test tests hashPassword method in PasswordHash.class
	 * Succeeds if the test can create correct hashed password.
	 */
	@Test
	void testHashPassword() {
		byte[] salt = passwordhash.generateUserSalt();
		byte[] hashedpassword = PasswordHash.hashPassword(password.toCharArray(), salt);
		assertEquals(hashedpassword, hashedpassword, "Hashed Password is incorect.");
	}
	
	/**
	 * Test compares hashed password to hashed password in database.
	 * Succeeds if botch hashed passwords are correct.
	 */
	@Test
	void testVerifyPasswordRight() {
		byte[] salt = passwordhash.generateUserSalt();
		byte[] hashedpassword = PasswordHash.hashPassword(password.toCharArray(),salt);
		boolean result = PasswordHash.verifyHashedPassword(password.toCharArray(),salt,hashedpassword);
		assertEquals(true,result,"Hashed password and database password are not same.");
	}
	
	/**
	 * Test compares hashed password to hashed password in database.
	 * Succeeds if password is not same password in database.
	 */
	@Test
	void testVerifyPasswordWrongPassword() {
		byte[] salt = passwordhash.generateUserSalt();
		byte[] hashedpassword = PasswordHash.hashPassword(password.toCharArray(),salt);
		boolean result = PasswordHash.verifyHashedPassword(wrongpassword.toCharArray(),salt,hashedpassword);
		//assertEquals ensimmäinen parametri voisi olla "Re-enter password", aluksi false.
		assertEquals(false,result, "Hashed password and database password are same.");
	}
	/**
	 * Tests method getUserSalt, test succeeds if test can get correct user salt and verifyHashedPassword returns true.
	 */
	@Test
	void testGetUserSalt() {
		byte[] salt = passwordhash.generateUserSalt();
		byte[] hashedpassword = PasswordHash.hashPassword(password.toCharArray(),salt);
		db = new GameTrain();
		User user = new User(0,"testi",hashedpassword.toString(),email);
		user.setSalt(salt);
		db.createUser(user);
		salt = passwordhash.getUserSalt(email);
		boolean result = PasswordHash.verifyHashedPassword(password.toCharArray(),salt,hashedpassword);
		assertEquals(true,result, "Salt was not correct.");
	}
	/**
	 * Test method getUserSalt with wrong email, test succeeds if method getUserSalt doesn't get correct salt and 
	 * returns false.
	 */
	@Test
	void testGetUserSaltWrongEmail() {
		byte[] salt = passwordhash.generateUserSalt();
		byte[] hashedpassword = PasswordHash.hashPassword(password.toCharArray(),salt);
		db = new GameTrain();
		User user = new User(0,"testii",hashedpassword.toString(),newemail);
		user.setSalt(salt);
		db.createUser(user);
		salt = passwordhash.getUserSalt(email);
		assertEquals(null,salt, "Salt was not null.");
	}
	
}
