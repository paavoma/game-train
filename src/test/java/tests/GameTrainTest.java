package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import model.GameTrain;
import model.User;
import model.UserStats;

class GameTrainTest extends AbstractParent{

	private static GameTrain gametrain = new GameTrain();
	private static User createtestUser = new User(0, "createtestUser",  "test", "createtest@test.com");
	private static User updatetestUser = new User(0, "updatetestUser",  "test", "test@test.com");
	private static User deleteTestUser = new User(0, "deleteUser",  "test", "deletetest@testi.com");
	private static User ReadByIduser = new User(0, "idUser",  "test", "idtest@test.com");
	private static User ReadByNameuser = new User(0, "NameUser",  "test", "nametest@test.com");
	private static User ReadByEmailuser = new User(0, "EmailUser",  "test", "emailtest@test.com");
	//private static UserStats teststats = new UserStats(0, null, 0, 0, 0,"wololoo",1,"Testi" ,null, null, gametrain.readUserByName("NameUser"),null);

	@BeforeAll
	static void fillDatabase() {
		gametrain.deleteUser(gametrain.readUserByName("idUser").getUserID());
		gametrain.deleteUser(gametrain.readUserByName("NameUser").getUserID());
		gametrain.deleteUser(gametrain.readUserByName("EmailUser").getUserID());
		gametrain.deleteUser(gametrain.readUserByName("updatetestUser").getUserID());
		gametrain.deleteUser(gametrain.readUserByName("deleteUser").getUserID());
		System.out.println("Tietokanta tyhjennetty jos ei ollut jo tyhjä");
		gametrain.createUser(deleteTestUser);
		gametrain.createUser(updatetestUser);
		gametrain.createUser(ReadByEmailuser);
		gametrain.createUser(ReadByIduser);
		gametrain.createUser(ReadByNameuser);
		//gametrain.createUserstat(teststats);
		System.out.println("Tietokanta täytetty testejä varten");
	}
	/*
	@AfterAll
	static void emptyDatabase() {
		gametrain.deleteUser(gametrain.readUserByName("idUser").getUserID());
		gametrain.deleteUser(gametrain.readUserByName("NameUser").getUserID());
		gametrain.deleteUser(gametrain.readUserByName("EmailUser").getUserID());
		gametrain.deleteUser(gametrain.readUserByName("updatetestUser").getUserID());
		gametrain.deleteUser(gametrain.readUserByName("deleteUser").getUserID());
		System.out.println("Tietokanta tyhjennetty jos ei ollut jo tyhjä");
	}
	*/
	
	@Disabled
	@Test
	void testCreateUser() {
		gametrain.createUser(createtestUser);
		assertTrue(true);
	}
	@Test
	void testReadUserById() {
		int userId = gametrain.readUserByName("idUser").getUserID();
		gametrain.readUserById(userId);
		String username = gametrain.readUserById(userId).getUsername();
		assertEquals("idUser", username, "Käyttäjätietojen haku meni pieleen");
	}
	@Test
	void testReadUserByName() {
		gametrain.readUserByName("NameUser");
		String userEmail = gametrain.readUserByName("NameUser").getEmail();
		assertEquals("nametest@test.com", userEmail, "Käyttäjätietojen haku meni pieleen");
	}
	@Test
	void testReadUserByEmail() {
		gametrain.readUserByEmail("emailtest@test.com");
		String userName = gametrain.readUserByEmail("emailtest@test.com").getUsername();
		assertEquals("EmailUser", userName, "Käyttäjätietojen haku meni pieleen");
	}
	@Disabled
	@Test
	void testUpdateUser() {
		User u = gametrain.readUserByName("updatetestUser");
		u.setEmail("updatetest@test.com");
		gametrain.updateUser(u);
		String email = gametrain.readUserByName("updatetestUser").getEmail();
		assertEquals("updatetest@test.com", email, "updating user details failed");
	}
	@Test
	void testDeleteUser() {
		int id = gametrain.readUserByName("deleteUser").getUserID();
		gametrain.deleteUser(id);
		assertTrue(true);
	}
	@Disabled
	@Test
	void testReadUsers() {
		
	}
	@Disabled
	@Test
	void testCreateGames() {
		
	}
	@Disabled
	@Test
	void testReadGames() {
		
	}
	@Disabled
	@Test
	void testUpdateGames() {
		
	}
	@Disabled
	@Test
	void testDeleteGames() {
		
	}
	@Disabled
	@Test
	void testReadAllGames() {
		
	}
	@Disabled
	@Test
	void testCreateUserStats() {
		
	}
	@Disabled
	@Test
	void testReadUserStats() {
	
	}
	/*
	@Disabled
	@Test
	void testReadUsersSessions() {
		gametrain.readUsersSessions(gametrain.readUserByName("NameUser").getUserID());
		assertEquals(1, gametrain.readUsersSessions(gametrain.readUserByName("NameUser").getUserID()), "Session haku epäonnistui");
	}
	*/
	@Disabled
	@Test
	void testUpdateUserStats() {
		
	}
	@Disabled
	@Test
	void testDeleteUserStats() {
		
	}
	@Disabled
	@Test
	void testReadAllUsersStats() {
		
	}
	
}
