package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Date;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import controller.GameSessionController;
import model.GSession;
import model.GameTrain;
import model.Games;
import model.PasswordHash;
import model.User;
import model.UserStats;
/**
 * This class is used to test GameSessionController methods.
 * @author Eelis Melto
 *
 */
class GameSessionControllerClassTest {
	private static Date now = new Date();
	private static PasswordHash hash = new PasswordHash();
	private static User user = new User(0,"pertti","salasana","email");
	private static Games game = new Games(0, "CSGO");
	private static GameTrain db = new GameTrain();
	private static GameSessionController gscontroller = new GameSessionController();
	private static GSession gsession = new GSession(0,"testisessio",null,null,null,null);
	private static GSession newgsession = new GSession(0,"toinensessio",null,null,null,null);
	private static UserStats userstats = new UserStats(0, null, 10, 5, 1, 0, 0, 0, 0, 0, 0, null, null,null,null);
	private static UserStats newuserstats = new UserStats(0, null, 10, 5, 1, 0, 0, 0, 0, 0, 0, null, null, null, null);
	private static UserStats updateduserstats = new UserStats(0, null, 15, 10, 0, 0, 0, 0, 0, 0, 0, null, null, null, null);
	private int kills = 10, deaths = 5, win = 1;
	private int newkills = 15, newdeaths = 10, newwin = 0;
	private String comments = "";
	private boolean succesfull;
	private int sessionid;
	
	/**
	 * Makes test user and test game for database.
	 */
	@BeforeAll 
	static void initializeTests() {
		String password = "salasana";
		char[] charpass = password.toCharArray();
		byte[] salt = hash.generateUserSalt();
		user.setProfpicture(null);
		user.setSalt(salt);
		user.setPassword((PasswordHash.hashPassword(charpass, salt)));
		db.createUser(user);
		db.createGames(game);
		user = db.readUserByName("pertti");
		game = db.readGameByName("CSGO");
		gsession.setUser(user);
		gsession.setGames(game);
		gsession.setStartTime(now);
		userstats.setGsession(gsession);
		updateduserstats.setGsession(gsession);
	}
	/**
	 * Deletes test session that was made in tests.
	 */
	@AfterEach
	void clearDatabase() {
		gscontroller.deleteGSession(sessionid);
	}
	/**
	 * After all test have been done, this deletes test user and test game from database.
	 */
	@AfterAll
	static void clearAllTestData() {
		db.deleteUser(user.getUserID());
		db.deleteGames(game.getGameID());
	}

	/**
	 * Tests method storeGSession.
	 * Test succeeds if test can properly store session to database.
	 */
	@Test
	void testStoreGSession() {
		userstats.setUserStatsID(0);
		userstats.getGsession().setSessionID(0);
		sessionid = gscontroller.storeGSession(userstats);
		int userstatsid = db.readUserStatsID(sessionid);
		UserStats savedsession = db.readUserstats(userstatsid);
		assertEquals(userstatsid, savedsession.getUserStatsID(),"storeGSession didn't return true.");
		assertEquals(kills, savedsession.getKills(), "Kills are incorrect");
		assertEquals(deaths, savedsession.getDeaths(), "Deaths are incorrect.");
		assertEquals(win, savedsession.getWin(), "Match result is incorrect.");
	}
	/**
	 * Tests method updateGSession.
	 * Test succeeds if test can properly update session in database.
	 */
	@Test
	void testUpdateGSession() {
		userstats.setUserStatsID(0);
		userstats.getGsession().setSessionID(0);
		GSession gs = userstats.getGsession();
		db.createGameSession(gs);
		sessionid = db.readLatestGameSession(gs.getUser().getUserID(),gs.getGames().getGameID());
		gs.setSessionID(sessionid);
		userstats.setGsession(gs);
		succesfull = db.createUserstat(userstats);
		//updateGSession parametrit tulevat olemaan (timestamp, sessionID, updateuserstats)
		updateduserstats.getGsession().setSessionID(sessionid);
		gscontroller.updateGSession(updateduserstats);
		UserStats session = gscontroller.getGSession(sessionid);
		assertEquals(newkills, session.getKills(),"Kills are incorrect.");
		assertEquals(newdeaths, session.getDeaths(), "Deaths are incorrect.");
		assertEquals(newwin, session.getWin(), "Match result is incorrect.");	
		
	}
	/**
	 * Tests getGSession method. 
	 * Test succeeds if test can get correct userstats/session from database.
	 */
	@Test
	void testGetGSession() {
		userstats.setUserStatsID(0);
		userstats.getGsession().setSessionID(0);
		GSession gs = userstats.getGsession();
		db.createGameSession(gs);
		sessionid = db.readLatestGameSession(gs.getUser().getUserID(),gs.getGames().getGameID());
		gs.setSessionID(sessionid);
		userstats.setGsession(gs);
		succesfull = db.createUserstat(userstats);
		UserStats session = gscontroller.getGSession(sessionid);
		assertEquals(userstats.getKills(),session.getKills(),"Kills are not same.");
		assertEquals(userstats.getDeaths(), session.getDeaths(), "Deaths are not same.");
	}
	
	/**
	 * Tests method getAllUsersGameSessions and getAllUsersGSessions.
	 * Test succeeds if test can properly get all users gamesessions from database.
	 */
	@Test
	void testGetAllUsersSessions() {
		userstats.setUserStatsID(0);
		userstats.getGsession().setSessionID(0);
		sessionid = gscontroller.storeGSession(userstats);
		GSession[] usersgsessions = gscontroller.getAllUsersGameSessions(userstats.getGsession().getUser().getUserID(), userstats.getGsession().getGames().getGameID());
		ArrayList<UserStats> sessions = gscontroller.getAllUsersGSessions(usersgsessions);
		assertEquals(1,usersgsessions.length, "Method getAllUsersGameSessions didn't get all gamesessions.");
		assertEquals(1, sessions.size(), "Method getAllUsersGSessions didn't get all userstats/sessions.");
	}
	
	/*'
	 * Tests method deleteGameSession.
	 * Test succeeds if test can properly delete right session.
	 */
	@Test
	void testDeleteGSession() {
		sessionid = gscontroller.storeGSession(userstats);
		succesfull = gscontroller.deleteGSession(sessionid);
		GSession deletedsession = db.readGameSession(sessionid);
		int deletedstats = db.readUserStatsID(sessionid);
		
		assertEquals(true,succesfull,"DeleteGsession didnt return true.");
		assertEquals(null,deletedsession.getUser(),"Gamesession was not deleted from database.");
		assertEquals(0, deletedstats, "UserStats didn't delete from database.");
	}
	
	/*'
	 * Tests method createGSessionComment.
	 * Test succeeds if test can properly create userstats with only information being comment and having correct sessionid.
	 */
	@Test
	void testCreateGSessionComment() {
		userstats.setUserStatsID(0);
		userstats.getGsession().setSessionID(0);
		sessionid = gscontroller.storeGSession(userstats);
		UserStats comment = new UserStats();
		comment.setComments("This is a test comment.");
		GSession gamesession = db.readGameSession(sessionid);
		succesfull = gscontroller.createGSessionComment(gamesession, comment);
		UserStats[] sessions = db.readUsersUserStatsWithSessionId(sessionid);
		assertEquals(true, succesfull, "Comment was not created to database.");
		assertEquals("This is a test comment.", sessions[1].getComments(), "Comments were not same.");
		
	}
}
