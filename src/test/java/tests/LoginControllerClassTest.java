package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import controller.LoginController;
import model.GameTrain;
import model.PasswordHash;
import model.User;

class LoginControllerClassTest extends AbstractParent {

	private static LoginController logincontroller = new LoginController();
	private static User user = new User();
	private static GameTrain gametrain = new GameTrain();
	private static String email = "test@test.com";
	private static String newemail = "newtest@newtest.com";
	private static String password = "test";
	private static String wrongpassword = "wrong";
	private static String name = "testname";
	private static PasswordHash passhash = new PasswordHash();
	
	/**
	 * This method ensures that database is empty of previous tests that might have gone wrong
	 */
	@BeforeAll
	static void deleteTestaccountsBefore() {

		gametrain.deleteUser(gametrain.readUserByEmail(newemail).getUserID());
		gametrain.deleteUser(gametrain.readUserByEmail(email).getUserID());

	}
	/**
	 * This method deletes accounts between tests
	 */
	@AfterEach
	void deleteTestAccountsAfterEach() {

		gametrain.deleteUser(gametrain.readUserByEmail(newemail).getUserID());
		gametrain.deleteUser(gametrain.readUserByEmail(email).getUserID());

	}

	
	/**
	 * Checks if registering new email works
	 */
	@Test
	void testRegisterUserEmailNew() {

		assertEquals(true, logincontroller.registerUser(name, password, newemail), "Couldn't create new user.");

	}

	
	/**
	 * Test New user registration where email is already in use, Succeeds if the
	 * test is not allowed to create new user
	 */
	@Test
	void testRegisterUserEmailExists() {

		gametrain.createUser(new User(0, name, password, email));

		assertEquals(false, logincontroller.registerUser(name, password, email),
				"Email already existed in the database and new user still was created.");

	}

	/**
	 * Test Login user method where @param Email,Password are both right. Succeeds
	 * if the test is allowed to login.
	 */	
	@Test
	void testLoginUserEmailFoundAndPasswordCorrect() {
		// Create a user to database
		User user = new User(0, name, password, email);
		char charpass[] = password.toCharArray();
		byte[] newsalt = passhash.generateUserSalt();
		byte[] hashedPassword = PasswordHash.hashPassword(charpass, newsalt);
		user.setPassword(hashedPassword);
		user.setSalt(newsalt);
		gametrain.createUser(user);

		// Create another user
		User userOther = new User(0, name, wrongpassword, newemail);
		char charpassOther[] = wrongpassword.toCharArray();
		newsalt = passhash.generateUserSalt();
		hashedPassword = PasswordHash.hashPassword(charpassOther, newsalt);
		userOther.setPassword(hashedPassword);
		userOther.setSalt(newsalt);
		gametrain.createUser(userOther);

		assertEquals(true, logincontroller.loginUser(email, password),
				"Login didn't succeed when email and password were right.");

	}

	/**
	 * Test Login user method with right email but password is wrong. Succeed if the
	 * test is not allowed login. Needs to get information that password was wrong.
	 */
	@Test
	void testLoginUserEmailFoundAndPasswordWrong() {
		// Create a user to database
		User user = new User(0, name, password, email);
		char charpass[] = password.toCharArray();
		byte[] newsalt = passhash.generateUserSalt();
		byte[] hashedPassword = PasswordHash.hashPassword(charpass, newsalt);
		user.setPassword(hashedPassword);
		user.setSalt(newsalt);
		gametrain.createUser(user);

		// Create another user
		User userOther = new User(0, name, wrongpassword, newemail);
		char charpassOther[] = wrongpassword.toCharArray();
		newsalt = passhash.generateUserSalt();
		hashedPassword = PasswordHash.hashPassword(charpassOther, newsalt);
		userOther.setPassword(hashedPassword);
		userOther.setSalt(newsalt);
		gametrain.createUser(userOther);

		assertEquals(false, logincontroller.loginUser(email, wrongpassword), "Login suceeded when password was wrong.");
	}

	/**
	 * Test login user method with wrong email. Succeeds if the test is not allowed
	 * to login and informs that email was wrong.
	 */
	@Test
	void testLoginUserEmailNotFound() {
		// Create a user to database
		User user = new User(0, name, password, email);
		char charpass[] = password.toCharArray();
		byte[] newsalt = passhash.generateUserSalt();
		byte[] hashedPassword = PasswordHash.hashPassword(charpass, newsalt);
		user.setPassword(hashedPassword);
		user.setSalt(newsalt);
		gametrain.createUser(user);

		// Create another user
		User userOther = new User(0, name, wrongpassword, newemail);
		char charpassOther[] = wrongpassword.toCharArray();
		newsalt = passhash.generateUserSalt();
		hashedPassword = PasswordHash.hashPassword(charpassOther, newsalt);
		userOther.setPassword(hashedPassword);
		userOther.setSalt(newsalt);
		gametrain.createUser(userOther);

		assertEquals(false, logincontroller.loginUser(newemail, password), "Login suceeded when password was wrong.");
	}

	

}