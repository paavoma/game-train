package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import controller.ProfileController;
import model.User;

class ProfileControllerTest {
	private ProfileController profilecontroller = new ProfileController();
	private User user = new User();
	
	private String email = "test@test.com";
	private String newemail = "newtest@newtest.com";
	private String wrongemail = "wrong@wrong.com";
	private String password = "test";
	private String wrongpassword = "wrong";
	private String name = "testname";
	private String newname = "newtestname";
	private String newpassword = "newpass";
	
	
	/*
	 * Tests method changeUserEmail().
	 * Test succeeds if method returns "Re-enter new email".
	 */
	@Disabled
	@Test
	void testChangeUserEmailUserEmailInUse() {
		/*
		 * assertEquals("Re-enter new email",profilecontroller.changeUserEmail(newemail,email,password),"User succeeded to change email to email that is already inuse");
		 */
	}
	/*
	 * Tests method changeUserEmail().
	 * Test succeeds if method returns "Re-enter email".
	 */
	@Disabled
	@Test
	void testChangeUserEmailUserEmailWrong() {
		/*
		 * assertEquals("Re-enter email",profilecontroller.changeUserEmail(newemail,wrongemail,password),"Users email changed when email was wrong.");
		 */
	}

	/*
	 * Tests method changeUserEmail.
	 * Test succeeds if method returns "Re-enter password".
	 */
	@Disabled
	@Test
	void testChangeUserEmailUserPasswordWrong() {
		/*
		 * assertEquals("Re-enter password",profilecontroller.changeUserEmail(newemail,email,wrongpassword),"Users email changed when password was wrong");
		 */
	}

	/*
	 * Tests method changeUserEmail().
	 * Test succeeds if method can change users email.
	 */
	@Disabled
	@Test
	void testChangeUserEmailSucceeds() {
		//profilecontroller.changeUserEmail(newemail,email,password)
		//assertEquals(user.getEmail,newemail,"Users email didn't change.");
		/*
		 * assertEquals(true,profilecontroller.changeUserEmail(newemail,email,password),"Users email didn't change.");
		 */
	}

	/*
	 * Tests method changeUserName().
	 * Test succeeds if method returns "Re-enter password".
 	 */
	@Disabled
	@Test
	void testChangeUserNamePasswordIncorect() {
		/*
		 * assertEquals("Re-enter password",profilecontroller.changeUserName(newname,wrongpassword),"Users profilename changed when password was incorect.");
		 */
	}

	/*
	 * Tests method changeUserName().
	 * Test succeeds if method can change users username.
	 */
	@Disabled
	@Test
	void testChangeUserNameSucceeds() {
		/*
		 * assertEquals(true,profilecontroller.changeUserName(newname,password),"Users profilename didn't change.");
		 */
	}

	/*
	 * Tests method changeUserImage().
	 * Test succeeds if method changes users profile image.
	 */
	@Disabled
	@Test
	void testChangeUserImage() {
		/*
		 * profilecontroller.changeUserImage(userId,newimage);
		assertEquals(user.getUserImage,newimage,"Users profile image didn't change.");
		 */
	}
	
	/*
	 * Test method getUserSnapShot().
	 * Test succeeds if method gets username, email and profile picture right.
	 */
	@Disabled
	@Test
	void createProfileView() {
		/*
		 * user.getUserSnapShot(userId);
		assertEquals(user.getUsername(),username,"SnapShot didn't get username.");
		assertEquals(user.getEmail(),email,"SnapShot didn't get email.");
		assertEquals(user.getProfpicture(),profpicture,"SnapShot didn't get profpicture.");
		 */
	}
	
	/*
	 * Tests method changeUserPassword()
	 * Test succeeds if method changes password
	 */
	@Disabled
	@Test
	void testChangeUserPasswordSucceeds() {
		/*
		 * assertEquals(true, profilecontroller.changeUserPassword(password, newpassword), "User password didn't change");
		 */
	}
	
	/*
	 *  Tests changeUserPassword()
	 *  Test succeeds if method asks current password again
	 */
	@Disabled
	@Test
	void testChangeUserPasswordCurrentPasswordIncorrect(){
		/*
		 * assertEquals("Re-enter password", profilecontroller.changeUserPassword(wrongpassword, newpassword), "User password changed with invalid current password");
		 */
	}

}
