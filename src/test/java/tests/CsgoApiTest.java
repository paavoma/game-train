package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import controller.GameSessionController;
import model.CsgoAPI;
import model.GSession;
import model.GameTrain;
import model.Games;
import model.InterfaceAPI;
import model.PasswordHash;
import model.User;
import model.UserStats;
/**
 * This class is used to test, CsgoAPI class methods.
 * @author Eelis Melto
 *
 */
class CsgoApiTest {
	private static Date now = new Date();	
	private static PasswordHash hash = new PasswordHash();
	private static User user = new User(0,"pertti","salasana","email");
	private static Games game = new Games(0, "CSGO");
	private static GameTrain db = new GameTrain();
	private static GameSessionController gscontroller = new GameSessionController();
	private static GSession gsession = new GSession(0,"testisessio",null,null,null,null);
	private static UserStats userstats = new UserStats(0, null, 10, 5, 1, 0, 0, 0, 0, 0, 0, null, null,null,null);
	private static UserStats updateduserstats = new UserStats(0, null, 25, 18, 1, 16, 11, 1, 3324, 90750, 50, null, null, null, null);
	public int sessionid;
	
	@BeforeAll 
	static void initializeTests() {
		String password = "salasana";
		char[] charpass = password.toCharArray();
		byte[] salt = hash.generateUserSalt();
		user.setProfpicture(null);
		user.setSalt(salt);
		user.setPassword((PasswordHash.hashPassword(charpass, salt)));
		db.createUser(user);
		db.createGames(game);
		user = db.readUserByName("pertti");
		game = db.readGameByName("CSGO");
		gsession.setUser(user);
		gsession.setGames(game);
		gsession.setStartTime(now);
		userstats.setGsession(gsession);
		updateduserstats.setGsession(gsession);
	}
	@AfterAll
	static void clearAllTestData() {
		db.deleteUser(user.getUserID());
		db.deleteGames(game.getGameID());
	}
	@AfterEach
	void clearDatabase() {
		gscontroller.deleteGSession(sessionid);
	}
	/**
	 * Tests method isThereNewMatch and database is empty so test should return true.
	 */
	@Test
	void testIsThereNewMatchDatabaseEmpty() {
		InterfaceAPI csgoapi = new CsgoAPI("76561198061586349", user);
		boolean isThereNewMatch = csgoapi.isThereNewMatch();
		assertEquals(true, isThereNewMatch,"There wasn't new match.");
		
	}
	
	/**
	 * Test isThereNewMatch and database is not empyty. 
	 */
	@Test
	void testIsThereNewMatchDatabaseNotEmpty() {
		InterfaceAPI csgoapi = new CsgoAPI("76561198061586349", user);
		sessionid = gscontroller.storeGSession(userstats);
		boolean isThereNewMatch = csgoapi.isThereNewMatch();
		assertEquals(true, isThereNewMatch, "There wasn't new match.");
	}
	/**
	 * Test isThereNewMatch and method should return false becouse there is allready same data 
	 * in database.
	 */
	@Test
	void testIsThereNewMatchDatabaseSame() {
		InterfaceAPI csgoapi = new CsgoAPI("76561198061586349", user);
		updateduserstats.getGsession().setSessionID(0);
		sessionid = gscontroller.storeGSession(updateduserstats);
		boolean isThereNewMatch = csgoapi.isThereNewMatch();
		assertEquals(false, isThereNewMatch, "Match data was not same with db and api.");
	}
	/**
	 * Tests isThereNewMatch method. Test tests methods function that it doesn't call database 
	 * if it's allready called it ones.
	 */
	@Test
	void testIsThereNewMatchNoDataBaseCall() {
		CsgoAPI csgoapi = new CsgoAPI("76561198061586349", user);
		sessionid = gscontroller.storeGSession(userstats);
		boolean isThereNewMatch = csgoapi.isThereNewMatch();
		boolean secondisThreNewMatch = csgoapi.isThereNewMatch();
		assertEquals(true, isThereNewMatch, "There was not new match when there should have been.");
		assertEquals(false, secondisThreNewMatch,"There was not new match wehen there should have been.");
		assertEquals(false, csgoapi.mLatestMatch.isEmpty(), "List was empty when it shouldn't");
		
	}
	/**
	 * Tests method that changes API data to userstats. 
	 * Test suceeds if method can change api data to userstats format.
	 */
	@Test
	void testChangeAPIDataToUserStats() {
		CsgoAPI csgoapi = new CsgoAPI("76561198061586349", user);
		boolean isThereNewMatch = csgoapi.isThereNewMatch();
		UserStats csgomatch = csgoapi.changeAPIDataToUserStats();
		sessionid = gscontroller.storeGSession(csgomatch);
		UserStats databasecsgomatch = gscontroller.getGSession(sessionid);
		assertEquals(csgomatch.getStat1(), databasecsgomatch.getStat1());
	}

}
