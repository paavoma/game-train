package controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import controller.GameSessionController;
import javafx.scene.chart.XYChart;
import model.GSession;
import model.User;
import model.UserStats;


/**
 * This class forms a series for javaFX charts from wanted stats from all game sessions
 * or specific timeframe of sessions
 * @author Paavo
 *
 */
public class ChartDataController {
	XYChart.Series<String, Number> series = null;
	private User user;
	private GameSessionController gsControl = new GameSessionController();
/**
 * Constructor for the class
 * @param user User object, used to get correct account data
 */
	public ChartDataController(User user) {
		this.user = user;
	}

	/**
	 * This method generates a XYChart.series from all of the sessions under gameID
	 * and stat
	 * @param gameID is the id of the game for the graph
	 * @param statname statistic to be searched
	 * @return XYChart series that is for the graph
	 */
	public XYChart.Series<String, Number> getAllSessionsSeries(int gameID, String statname) {
		ArrayList<UserStats> sessions = getAllSessions(gameID, statname);
		return getGeneratedSeries(sessions, statname);
	}

	/**
	 * This method parses a list of UserStats (statistics) and forms the XYChart series from the wanted stat
	 * 
	 * @param sessions list of UserStats
	 * @param statname statistic to be searched
	 * @return XYChart series that is for the graph
	 */
	public XYChart.Series<String, Number> getGeneratedSeries(ArrayList<UserStats> sessions, String statname) {
		series = new XYChart.Series<String, Number>();
		String sessionnumber = null;
		//Date previousDate = sessions.get(0).getDate();
		int previousStat = 0;
		int index = 1;

		for (UserStats session : sessions) {
			if (session.getComments() == null) {
				String foundStat = null;
				// poistetaan turhat merkit, jotta löydetään etsittävä statti
				String statsString = session.toString();
				String[] splitted = statsString.split("=|[, ]|\\[|\\]");

				for (int i = 0; i < splitted.length; i++) {
					//System.out.println(splitted[i]);
					if (splitted[i].equals(statname)) {
						foundStat = splitted[i + 1];
					}
				}
				// just to name stats;
				if (session.getDate() == null) {
					sessionnumber = "Session" + Integer.toString(index);
					if (foundStat != null)
						series.getData()
								.add(new XYChart.Data<String, Number>(sessionnumber, Integer.parseInt(foundStat)));
				}

				else {
				}
				//previousDate = session.getDate();
				previousStat = Integer.parseInt(foundStat);
				index++;
			}
		}
		return series;
	}
	/**
	 * This method is used to generate series of ratio of two statistics from given UserStats list statname1/statname2
	 * 
	 * @param sessions list of UserStats
	 * @param statname1 statistic to compare
	 * @param statname2 statistic to divide with
	 * @return XYChart series that is for the graph
	 */

	public XYChart.Series<String, Number> getGeneratedSeries(ArrayList<UserStats> sessions, String statname1,
			String statname2) {
		series = new XYChart.Series<String, Number>();
		String sessionnumber = null;
		//Date previousDate = sessions.get(0).getDate();
		int previousStat1 = 0;
		int previousStat2 = 0;
		int index = 1;
		ArrayList<Double> ratios = new ArrayList<Double>();
		for (UserStats session : sessions) {
			String foundStat1 = null;
			String foundStat2 = null;
			
			String statsString = session.toString(); // poistetaan turhat merkit, jotta löydetään etsittävä statti
			String[] splitted = statsString.split("=|[, ]|\\[|\\]");

			for (int i = 0; i < splitted.length; i++) {
				//System.out.println(splitted[i]);
				if (splitted[i].equals(statname1)) {
					foundStat1 = splitted[i + 1];
				}
				if (splitted[i].equals(statname2)) {
					foundStat2 = splitted[i + 1];
				}
			}
			// just to name stats;
			double ratio = (double) Integer.parseInt(foundStat1) / (double) Integer.parseInt(foundStat2);
			System.out.println("Tässä on ratio: " + ratio);
			if (session.getDate() == null) {
				sessionnumber = Integer.toString(index);
				if (foundStat1 != null)
					series.getData().add(new XYChart.Data<String, Number>(sessionnumber, ratio));
			}
												
			index++;
		}
		return series;
	}

	/**
	 * This method returns a XYChart.Series String, Number between X number of days
	 * 
	 * @param gameID id for the game to search
	 * @param numberOfDays days to count for the graph
	 * @param statname statistic to be tracked
	 * @return XYChart series that is for the graph
	 */
	public XYChart.Series<String, Number> getSeriesFromLastDays(int gameID, int numberOfDays, String statname) {
		ArrayList<UserStats> sessions = getAllSessions(gameID, statname);
		ArrayList<UserStats> sessionsAfter = new ArrayList<UserStats>();
		for (UserStats session : sessions) {
			// Filters sessions that are within given daylimit
			Date date = session.getDate();
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			int d = numberOfDays * -1; // In order to reduce date, needs to be negative
			c.add(Calendar.DATE, d);
			date = c.getTime();
			if (date.after(session.getDate())) {
				sessionsAfter.add(session);
			}
		}
		return getGeneratedSeries(sessionsAfter, statname);
	}
	
	/**
	 * This method gets ratio statistics from x days
	 * @param gameID the gameID in database
	 * @param numberOfDays days to track
	 * @param statname1 statistic to be divided for the ratio
	 * @param statname2 statistic to divide with
	 * @return generated data series for the chart
	 */

	public XYChart.Series<String, Number> getRatioSeriesFromLastDays(int gameID, int numberOfDays, String statname1,
			String statname2) {
		ArrayList<UserStats> sessions = getAllSessions(gameID, statname1);
		ArrayList<UserStats> sessionsAfter = new ArrayList<UserStats>();
		for (UserStats session : sessions) {
			if(session.getComments() == null)
				sessionsAfter.add(session);
		}
		return getGeneratedSeries(sessionsAfter, statname1, statname2);
	}
	
	/**
	 * This method asks all the sessions from the player under given ID
	 * @param gameID the gameID in database
	 * @param statname statistic to be tracked
	 * @return list of all session statistics from the player
	 */
	public ArrayList<UserStats> getAllSessions(int gameID, String statname) {
		GSession[] gsessions;
		gsessions = gsControl.getAllUsersGameSessions(user.getUserID(), gameID);
		return gsControl.getAllUsersGSessions(gsessions);
	}
	
	/**
	 * Getter for dataseries
	 * @return series for the graph
	 */
	public XYChart.Series getChartData() {
		return series;
	}

}
