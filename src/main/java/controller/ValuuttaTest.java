package controller;

import javax.persistence.*;

/**
 * @author Eelis Melto
 */
 
@Entity
@Table(name="VALUUTTA")
public class ValuuttaTest {
	
	@Id 
	@Column(name="tunnus")
	private String tunnus;
	
	@Column(name="nimi")
	private String nimi;
	
	@Column(name="vaihtokurssi")
	private double vaihtokurssi;
	
	public ValuuttaTest(String tunnus, double vaihtokurssi, String nimi) {
		super();
		this.tunnus = tunnus;
		this.nimi = nimi;
		this.vaihtokurssi = vaihtokurssi;
	}
	
	public ValuuttaTest() {
		super();
	}
	
	public void setTunnus(String tunnus) {
		this.tunnus = tunnus;
	}
	public String getTunnus() {
		return tunnus;
	}
	public void setVaihtokurssi(double vaihtokurssi) {
		this.vaihtokurssi = vaihtokurssi;
	}
	public double getVaihtokurssi() {
		return vaihtokurssi;
	}
	public void setNimi(String nimi) {
		this.nimi = nimi;
	}
	public String getNimi() {
		return nimi;
	}
}
