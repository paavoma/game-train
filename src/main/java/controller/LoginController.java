package controller;

import model.GameTrain;
import model.PasswordHash;
import model.User;

public class LoginController {
	GameTrain gametrain = null;
	PasswordHash passhash = null;
	private String databaseemail = "default";
	private String databasepass = "default";
	
	public LoginController() {
		this.gametrain = new GameTrain();
		this.passhash = new PasswordHash();
	}

	/**
	 * Registers a new user if given email is not in database
	 * @param name User name for the account
	 * @param password password for the account as String
	 * @param email email address for the account as String
	 * @return boolean if successful
	 */
	public boolean registerUser(String name, String password, String email) {

		databaseemail = gametrain.readUserByEmail(email).getEmail();
		System.out.println("Email in database: " + databaseemail);

		if (databaseemail == null && email != null) {

			User user = new User(0, name, password, email);
			char charpass[] = password.toCharArray();
			byte[] newsalt = passhash.generateUserSalt();
			byte[] hashedPassword = PasswordHash.hashPassword(charpass, newsalt);
			user.setPassword(hashedPassword);
			user.setSalt(newsalt);
			gametrain.createUser(user);

			System.out.println("Account create successful");
			return true;

		} else if (databaseemail.equals(email)) {

			System.out.println("Email already in use");
			return false;

		} else {
			System.out.println("Something mysterious happenoid");
			return false;
		}

	}
	
	/**
	 * 
	 * Performs a login by comparing given parameters with database
	 * and returns success 
	 * 
	 * @param password password for the account as String
	 * @param email email address for the account as String
	 * @return boolean if successful
	 */
	public boolean loginUser(String email, String password) {

		databaseemail = gametrain.readUserByEmail(email).getEmail();
		System.out.println("Salasana on: " + databasepass);

		System.out.println("Email in database: " + databaseemail);
		if (databaseemail == null) {

			System.out.println("Please check your email address");
			return false;

		} else if (databaseemail.equals(email) && databaseemail != null) {

			char charpass[] = password.toCharArray();

			System.out.println(email + " password in database: " + databaseemail);

			if (PasswordHash.verifyHashedPassword(charpass, gametrain.readUserByEmail(email).getSalt(),
					gametrain.readUserByEmail(email).getPassword()) && databaseemail.equals(email)) {
				System.out.println("Login succesful");
				return true;
			} else {
				System.out.println(email + " password in database: " + databaseemail);
				System.out.println("Re-enter password");
				return false;
			}

		} else {
			System.out.println("Something wierd happened");
			return false;
		}

	}

}