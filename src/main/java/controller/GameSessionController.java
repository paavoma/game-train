package controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.hibernate.stat.Statistics;

import model.GSession;
import model.GameSession;
import model.GameTrain;
import model.Games;
import model.User;
import model.UserStats;
/**
 * This class is used as a middleman between view and model. It uses GameTrain class methods
 * And returns their results. Also this class is used to parse UserStats objects to GameSession objects and
 * GameSession objects to UserStats objects.
 * @author Eelis Melto
 *
 */
public class GameSessionController {
	/** The database connection*/
	private GameTrain db;
	
	/** The gamesession */
	private GSession gsession;
	
	/** The userstats*/
	private UserStats session;
	
	/** The Array of UserStats */
	private UserStats[] sessionwithcomment;
	
	/** The Array of GSessions */
	private GSession[] gamesessions;
	
	/** The ArrayList of UserStats */
	private ArrayList<UserStats> sessions = new ArrayList<>();
	
	/** true or false boolean */
	private boolean succesfull;
	
	/**
	 * GameSessionController constructor.
	 */
	public GameSessionController() {
		this.db = new GameTrain();
		this.session = new UserStats();
	}
	
	/**
	 * Stores userstats/session to database.
	 * @param session is UserStats that will be saved to database. 
	 * Method first saves sessions gsession. Then asks what gsessions sessionid is and then gives it to session.
	 * Finally session will be stored to database.
	 * @return sessionid - witch is int value that is used to know what sessionid value UserStats has in database.
	 */
	public int storeGSession(UserStats session) {
		this.session = session;
		int sessionid = 0;
		try {
			GSession gs = this.session.getGsession();
			db.createGameSession(gs);
			sessionid = db.readLatestGameSession(gs.getUser().getUserID(), gs.getGames().getGameID());
			gs.setSessionID(sessionid);
			this.session.setGsession(gs);
			succesfull = db.createUserstat(this.session);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sessionid;
	}
	
	/**
	 * This method updates UserStats in database. 
	 * @param  updatedsession is UserStats that has updated information. 
	 * Needs to have same GSession with Databases UserStats.
	 * @return succesfull - boolean witch tells did UserStats update correctly.
	 */
	public boolean updateGSession(UserStats updatedsession) {
		this.session = updatedsession;
		int userstatsid = db.readUserStatsID(this.session.getGsession().getSessionID());
		session.setUserStatsID(userstatsid);
		succesfull = db.updateUserstats(session);
		return succesfull;
	}
	
	/**
	 * Metodin tarkoitus että palauttaa yhden pelisession tapahtumien tiedot.
	 * 
	 * This method returns single UserStats. That contains sessions stats.
	 * Method doesn't return comments only stats.
	 * @param sessionid is int value. This value is used to get correct UserStats.
	 * @return UserStats - witch has correct @param sessionid.
	 */
	public UserStats getGSession(int sessionid) {
		int userstatsid = db.readUserStatsID(sessionid);
		if (userstatsid == 0) {
			return null;
		} else {
			session = db.readUserstats(userstatsid);
			return session;
		}
	}
	
	/**
	 * This method returns array of UserStats[]. Array contains one session information that can 
	 * contain one or more UserStats.
	 * If this method contains more than one userstats it means that this session has comments.
	 * @param sessionid is int value. This value is used to get correct UserStats.
	 * @return sessionwithcomment - array of UserStats[] and all all UserStats in array have same @param sessionid.
	 */
	public UserStats[] getGSessionWithComments(int sessionid) {
		sessionwithcomment = db.readUsersUserStatsWithSessionId(sessionid);
		return sessionwithcomment;
		
	}
	
	/**
	 *This method is used to get users GSession in one game that is last saved to database.
	 *@param userID - is int value that is unique to user.
	 *@param gameID - is int value that is unique to game.  
	 *@return GSession - object.
	 *or @return null - if there is no GSession stored.
	 */
	public UserStats getLatestGSession(int userID, int gameID) {
		int sessionid = this.db.readLatestGameSession(userID, gameID);
		if (sessionid == 0) {
			return null;
		}
		return this.getGSession(sessionid);
	}
	/**
	 * Metodin tarkoitus että palauttaisi kaikki yhden pelin sessiot.
	 * 
	 * This method is used to return all users one games GameSessions from database (not UserStats/sessions).
	 * Needs two parameters to work.
	 * @param userID - int value that is unique to user. 
	 * @param gameID - int value that is unique to game.
	 * @return gamesession - GSession[] array that contains all gamesessions player has on one game. 
	 */
	public GSession[] getAllUsersGameSessions(int userID, int gameID) {
		try {
			gamesessions = db.readUsersSessions(userID, gameID);
		} catch (Exception e) {
			e.printStackTrace();
			gamesessions=null;
		}
		return gamesessions;
	}
	
	/**
	 * This method returns all userstats/sessions that link to users gamesessions in database. 
	 * @param gamesessions - GSession[] array to work. 
	 * @return sessions - ArrayList UserStats that contains all UserStats user has.
	 */
	public ArrayList<UserStats> getAllUsersGSessions(GSession[] gamesessions) {
		UserStats[] userstats = null;
		try {
			for (GSession gSession : gamesessions) {
				userstats = db.readUsersUserStatsWithSessionId(gSession.getSessionID());
				for (UserStats userStats2 : userstats) {
					sessions.add(userStats2);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			sessions = null;
		}
		return sessions;
	}
	/**
	 * Deletes one GameSession row from database and one UserStats row from database.
	 * @param sessionid - int value used to find correct GameSession and UserStats from database.
	 * @return succesfull - boolean that can be false if GameSessuon and UserStats was not found or deleted,
	 * Or true if correct GameSession and UserStats was deleted.
	 */
	public boolean deleteGSession(int sessionid) {
		succesfull = db.deleteUserStatsWithSessionID(sessionid);
		if (succesfull != false) {
			succesfull = db.deleteSession(sessionid);
		}
		return succesfull;
	}
	/**
	 * Method that creates userstats that is only used to keep comment.
	 * @param gsession is used to link correct session with comment.
	 * @param sessionwithcomment a session with comments in it
	 * @return a boolean, if commenting is succesful
	 */
	public boolean createGSessionComment(GSession gsession,UserStats sessionwithcomment) {
		this.gsession = gsession;
		this.session = sessionwithcomment;
		session.setGsession(this.gsession);
		succesfull = db.createUserstat(sessionwithcomment);
		return succesfull;
	}
	/**
	 * Method that parses UserStats for view. It gives UserStats parameters to GameSession object.
	 * @param userstats - array of UserStats. 
	 * @return newsession - GameSession object that is used in view.
	 */
	public GameSession parseUserStatsToGameSession(UserStats[] userstats) {
		GameSession newsession;
		if (userstats[0].getGsession().getGames().getGameName() == "CSGO") {
			newsession = new GameSession(1);
		}
		else if (userstats[0].getGsession().getGames().getGameName() == "LOL") {
			newsession = new GameSession(2);
		}
		else {
			newsession = new GameSession(3);
		}
		newsession.setGametitle(userstats[0].getGsession().getGames().getGameName());
		newsession.setDatabaseSessionID(userstats[0].getGsession().getSessionID());
		ArrayList<String> stats = new ArrayList<String>();
		stats.add(Integer.toString(userstats[0].getKills()));
		stats.add(Integer.toString(userstats[0].getDeaths()));
		stats.add(Integer.toString(userstats[0].getWin()));
		stats.add(Integer.toString(userstats[0].getStat1()));
		stats.add(Integer.toString(userstats[0].getStat2()));
		stats.add(Integer.toString(userstats[0].getStat3()));
		stats.add(Integer.toString(userstats[0].getStat4()));
		stats.add(Integer.toString(userstats[0].getStat5()));
		stats.add(Integer.toString(userstats[0].getStat6()));
		newsession.setStats(stats);
		ArrayList<String> notes = new ArrayList<String>();
		for (int i = 0; i < userstats.length; i++) {
			notes.add(userstats[i].getComments());
		}
		newsession.setNotes(notes);
		return newsession;
	}
	/**
	 * Method that gives GameSession objects parameters to UserStats object. Used when GameSession object
	 * is wanted in a format that can be saved to database.
	 * @param session - GameSession object that is wanted as UserStats object.
	 * @param user - User object, used to link UserStats to correct User.
	 * @return userstatsArray - UserStats array that contains one UserStats that contains Statistics, 
	 * and all other UserStats contain comments that User has made. 
	 */
	public UserStats[] parseGameSessionToUserStats(GameSession session, User user) {
		Games game;
		if (session.getGameID() == 1) {
			game = db.readGameByName("CSGO");
		}
		else if (session.getGameID() == 2) {
			game = db.readGameByName("LOL");
		}
		else {
			game = db.readGameByName("DOTA2");
		}
		GSession gs = new GSession(0, "", null, null, user, game);
		UserStats userstats = new UserStats();
		ArrayList<String> stats = new ArrayList<String>();
		ArrayList<String> notes = new ArrayList<String>();
		stats = session.getStats();
		notes = session.getNotes();
		userstats.setKills(Integer.parseInt(stats.get(0)));
		userstats.setDeaths(Integer.parseInt(stats.get(1)));
		userstats.setWin(Integer.parseInt(stats.get(2)));
		userstats.setStat1(Integer.parseInt(stats.get(3)));
		userstats.setStat2(Integer.parseInt(stats.get(4)));
		userstats.setStat3(Integer.parseInt(stats.get(5)));
		userstats.setStat4(Integer.parseInt(stats.get(6)));
		userstats.setStat5(Integer.parseInt(stats.get(7)));
		userstats.setGsession(gs);
		if (!notes.isEmpty()) {
			UserStats[] userstatsArray = new UserStats[notes.size() + 1];
 			for (int i = 0; i < userstatsArray.length; i++) {
 				UserStats comment = new UserStats();
 				comment.setGsession(gs);
 				if (notes.size() <= i) {
 					
 				} else {
 					comment.setComments(notes.get(i));
 					userstatsArray[i + 1] = comment;
 				}
			}
 			userstatsArray[0] = userstats;
 			return userstatsArray;
		} else {
			UserStats[] userstatsArray = new UserStats[1];
			userstatsArray[0] = userstats;
			return userstatsArray;
		}
			
	}
} 
