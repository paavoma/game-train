package controller;

import javax.persistence.*;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.sql.Connection;

public class HibernateTest {
	private SessionFactory istuntotehdas = null;
	
	private final StandardServiceRegistry registry = 
			new StandardServiceRegistryBuilder().configure().build();
	
	
	/*
	 * Konstruktori joka luo yhteyden MySQL tietokantaan.
	 */
	public HibernateTest() {
		try {
			istuntotehdas = 
					new MetadataSources(registry).buildMetadata().buildSessionFactory();
		}
		catch (Exception e) {
			System.out.println("Istuntotehtaan luonti epäonnistui.");
			StandardServiceRegistryBuilder.destroy(registry);
			e.printStackTrace();
			System.exit(-1);
		}
	}
	
	/*
	 * Metodi joka luo valuutan tietokantaan.
	 * Käyttäkää esimerkkinä!
	 */
	public boolean createValuutta(ValuuttaTest valuutta) {
		Transaction transaktio = null;
		ValuuttaTest v = valuutta;
		try (Session istunto = istuntotehdas.openSession()) {
			transaktio = istunto.beginTransaction();
			istunto.saveOrUpdate(v);
			transaktio.commit();
			return true;
		} catch (Exception e) {
			if (transaktio != null) {
				transaktio.rollback();
				throw e;
			}
			return false;
		}
		
	}
	/*
	 * Metodi hakee tietokannasta tunnuksella valuutan.
	 * Käyttäkää esimerkkinä!
	 */
	public ValuuttaTest readValuutta(String tunnus) {
		ValuuttaTest valuutta = new ValuuttaTest();
		try (Session istunto = istuntotehdas.openSession()) {
			istunto.beginTransaction();
			
			
			istunto.load(valuutta, tunnus);
			System.out.println(valuutta.getVaihtokurssi());
			
			istunto.getTransaction().commit();
			istunto.close();
						
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return valuutta;
	}
	
	/*
	 * Metodi päivittää tietokannassa olevaa valuuttaa.
	 * @param valuutta anetaan valuutta joka on päivitetty.
	 * Käyttäkää esimerkkinä! 
	 */
	public boolean updateValuutta(ValuuttaTest valuutta) {
		
		try (Session istunto = istuntotehdas.openSession()) {
			istunto.beginTransaction();
			ValuuttaTest v = (ValuuttaTest)istunto.get(ValuuttaTest.class, valuutta.getTunnus());
			if (v != null) {
				v.setVaihtokurssi(valuutta.getVaihtokurssi());
				v.setNimi(valuutta.getNimi());
			} else {
				System.out.println("Päivitettävää ei löytynyt.");
				return false;
			}
			istunto.getTransaction().commit();
			istunto.close();
		}
		return true;
	}
	
	/*
	 * Poistaa tietokannasta valuutan jonka tunnus on metodiin annettava parametri.
	 * Käyttäkää esimerkkinä!
	 */
	public boolean deleteValuutta(String tunnus) {
		try (Session istunto = istuntotehdas.openSession()) {
			istunto.beginTransaction();
			ValuuttaTest v = (ValuuttaTest)istunto.get(ValuuttaTest.class, tunnus);
			if (v != null) {
				istunto.delete(v);
			} else {
				System.out.println("Valuutan poistaminen ei onnistunut.");
				return false;
			}
			istunto.getTransaction().commit();
			istunto.close();
		}
		return true;
	}	
	
	/*
	 * Tulostaa kaikki valuutat tietokannasta.
	 * Käyttäkää esimerkkinä!
	 */
	public ValuuttaTest[] readValuutat() {
		try (Session istunto = istuntotehdas.openSession()) {
			istunto.beginTransaction();
			@SuppressWarnings("unchecked")
			List<ValuuttaTest> result = istunto.createQuery("from Valuutta").getResultList();
			for (ValuuttaTest valuutta : result) {
				System.out.println("Valuutta ("+ valuutta.getTunnus() +") : "+valuutta.getNimi()+" :"+valuutta.getVaihtokurssi()+" ");
			}
			
			istunto.getTransaction().commit();
			istunto.close();
			
			ValuuttaTest[] returnlist = new ValuuttaTest[result.size()];
			
			return (ValuuttaTest[])result.toArray(returnlist);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			
		}
		
		
		
	}


}
