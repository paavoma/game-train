package view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;

import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import jfxtras.styles.jmetro8.JMetro;
import model.GameTrain;
import model.Games;
import view.GametrainController;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;



import controller.LoginController;
import javafx.event.ActionEvent;

import javafx.scene.control.Label;

import javafx.scene.control.PasswordField;

public class LoginWindowController {
	/** Locale */
	private Locale currentLocale = new Locale("en", "GB", "UTF-8");
	/** ResourceBundle for the localization */
	private ResourceBundle texts = ResourceBundle.getBundle("TextResources", currentLocale);
	/** Database controller */
	private GameTrain db = new GameTrain();
	/** Login controller */
	private LoginController logincontroller = new LoginController();
	@FXML
	private PasswordField passwordfieldPassword;
	@FXML
	private TextField textfieldEmail;
	@FXML
	private Label labelEmail;
	@FXML
	private Label labelPassword;
	@FXML
	private Button buttonRegister;
	@FXML
	private Button buttonLogin;
	
	@FXML
	private Label labelGametrain;
	@FXML
	private Label labelWarningEmail;
	@FXML
	private Label labelWarningPassword;
	/*
	 * Preparations for displaying login window.
	 */
	public void initialize() {
		currentLocale = new Locale("en", "GB", "UTF-8");
		texts = ResourceBundle.getBundle("TextResources");
		textfieldEmail.requestFocus();
		labelWarningEmail.setTextFill(Color.web("#b10202"));
		labelWarningPassword.setTextFill(Color.web("#b10202"));
		labelWarningEmail.setText(null);
		labelWarningPassword.setText(null);

	}

	/** Event Listener on Button[#buttonRegister].onAction
	 *  @param event passed action event
	 *  
	 */
	@FXML
	public void handleRegisterButton(ActionEvent event) {
		clearWarningLabels();
		String inputemail = textfieldEmail.getText();
		String inputpass = passwordfieldPassword.getText();

		if (isValid(inputemail) == false || inputpass == null || inputpass.equals("")) {
			if(isValid(inputemail) == false) {
			
				labelWarningEmail.setText(texts.getString("messageRegisterCheckFormat"));
			}
			if (inputpass == null || inputpass.equals("")) {
				
				labelWarningPassword.setText(texts.getString("messageRegisterCheckPassword"));
			}
		} else {

			if (logincontroller.registerUser("default", inputpass, inputemail) == false) {
				clearWarningLabels();
				textfieldEmail.setText(inputemail);
				passwordfieldPassword.setText(null);
				System.out.println(texts.getString("messageRegisterFailed"));
				
				//prints an alert
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setHeaderText(null);
				alert.setTitle(texts.getString("messageRegisterFailed"));
				alert.setContentText(texts.getString("messageEmailInUse"));
				alert.showAndWait();
				
			} else {
				
				passwordfieldPassword.setText(null);
				
				System.out.println(texts.getString("messageRegisterSuccess"));
				Games csgo = new Games(0, "CSGO");
				db.createGames(csgo);

			}
		}
	}

	/** Event Listener on Button[#buttonLogin].onAction 
	 *  @param event passed action event
	 * */
	@FXML
	public void handleLoginButton(ActionEvent event) {
		clearWarningLabels();
		if(isValid(textfieldEmail.getText()) == false) {
			labelWarningEmail.setText(texts.getString("messageRegisterCheckFormat"));
		
		} else if (logincontroller.loginUser(textfieldEmail.getText(), passwordfieldPassword.getText()) == true) {
			
			try {
				Locale currentLocale = new Locale("en", "GB");
				
				ResourceBundle texts = ResourceBundle.getBundle("TextResources", currentLocale);
				
				
		        FXMLLoader fxmlLoader = new FXMLLoader();
		        fxmlLoader.setLocation(getClass().getResource("Gametrain.fxml"));
		        fxmlLoader.setResources(texts);
		        //fxmlLoader.load(getClass().getResource("Gametrain.fxml"), texts);
		        //opens up main app view
		        JMetro metroStyle = new JMetro(JMetro.Style.LIGHT);
		        Scene scene = new Scene(fxmlLoader.load(), 964, 600);
		        GametrainController gtcontroller = fxmlLoader.getController();
		        gtcontroller.initialize(db.readUserByEmail(textfieldEmail.getText()), currentLocale);
		        Stage stage = new Stage();
		        stage.setTitle("Gametrain");
		        stage.setScene(scene);
		        metroStyle.applyTheme(scene); 
		        stage.show();
		        
		        //closes the login window
		        Stage stageToClose = (Stage) buttonLogin.getScene().getWindow();
		        stageToClose.hide();
		    } catch (IOException e) {
		        Logger logger = Logger.getLogger(getClass().getName());
		        logger.log(Level.SEVERE, "Failed to create new Window.", e);
		    }
			
		}
		else {
			
			passwordfieldPassword.setText(null);
			labelWarningPassword.setText(texts.getString("messageCheckUserPassword"));
			
		}
	}

	/** Event Listener on Button[#buttonCancel].onAction 
	 *  @param event passed action event
	 */
	@FXML
	public void handleCancelButton(ActionEvent event) {
		
	}

	/**
	 *  Method that tests that given String is in valid form: **@**.**
	 *  @param email is the String to check
	 *  @return true if input is valid
	 */
	public static boolean isValid(String email) {
		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." + "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
				+ "A-Z]{2,7}$";

		Pattern pat = Pattern.compile(emailRegex);
		if (email == null)
			return false;
		return pat.matcher(email).matches();
	}
	/**
	 * Clears textfield labels
	 */
	public void clearWarningLabels() {
		labelWarningEmail.setText(null);
		labelWarningPassword.setText(null);
	}

}
