package view;

import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.stage.Stage;
import jfxtras.styles.jmetro8.JMetro;
import javafx.scene.*;
import javafx.fxml.*;

/**
 * Main class for the GameTrain program
 * @author Paavo
 *
 */
public class maintest extends Application {

	public GametrainController controller = new GametrainController();

	@Override
	public void start(Stage primaryStage) throws Exception {
		JMetro metroStyle = new JMetro(JMetro.Style.LIGHT);
		Locale currentLocale = new Locale("en", "GB");
		ResourceBundle texts = ResourceBundle.getBundle("TextResources", currentLocale);
		primaryStage.setTitle("GameTrain"); 
		Parent root = FXMLLoader.load(getClass().getResource("LoginWindow.fxml"), texts);
		primaryStage.setScene(new Scene(root, 500, 200));

		metroStyle.applyTheme(primaryStage.getScene()); 
		primaryStage.show();


	}

	public static void main(String[] args) {
		launch(args);
	}

}
