package view;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import controller.ChartDataController;
import controller.GameSessionController;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.Window;
import jfxtras.styles.jmetro8.JMetro;
import model.API;
import model.CsgoAPI;
import model.GSession;
import model.GameSession;
import model.GameTrain;

import model.GuiFactory;
import model.IGuiFactory;
import model.InterfaceAPI;
import model.User;
import model.UserStats;

/**@author Ninja
 * Controls the GUI.
 */
public class GametrainController {

	/** The tabpane. */
	@FXML
	private TabPane tabpane;

	/** The tab 1. Profile */
	@FXML
	private Tab tab1;
	
	/** The tab 2. Game 1 */
	@FXML
	private Tab tab2;
	
	/** The tab 3. Game 2 */
	@FXML
	private Tab tab3;
	
	/** The tab 4. Game 3 */
	@FXML
	private Tab tab4;
	
	/** The tab 5. Add game */
	@FXML
	private Tab tab5;

	/** The cs button savenotes. */
	@FXML
	private Button csbuttonStartsession;
	
	/** The csbutton end session. */
	@FXML
	private Button csbuttonEndSession;
	
	/** The cs button savenotes. */
	@FXML
	private Button csButtonSavenotes;
	
	/** The delete cs session button. */
	@FXML
	private Button deleteCsSessionButton;

	/** The cs text area notes. Where the user writes their notes. */
	@FXML
	private TextArea csTextAreaNotes;

	/** The csgogridpane. Where CsGo data is shown */
	@FXML
	private GridPane csgogridpane;

	/** The dotabutton savenotes. Saves the note written in dotaTextAreaNotes */
	@FXML
	private Button dotabuttonStartsession; 
	
	/** The dotabutton end session. */
	@FXML
	private Button dotabuttonEndSession; 
	
	/** The dotabutton savenotes. */
	@FXML
	private Button dotabuttonSavenotes;
	
	/** The delete dota session button. */
	@FXML
	private Button deleteDotaSessionButton;

	/** The lolbutton startsession. */
	@FXML
	private Button lolbuttonStartsession;
	
	/** The lolbutton end session. */
	@FXML
	private Button lolbuttonEndSession;
	
	/** The lolbutton savenotes. */
	@FXML
	private Button lolbuttonSavenotes;
	
	/** The delete lol session button. */
	@FXML
	private Button deleteLolSessionButton;

	/** The dota text area notes. Where the user writes their notes. */
	@FXML
	private TextArea dotaTextAreaNotes;

	/** The lol text area notes. */
	@FXML
	private TextArea lolTextAreaNotes;

	/** The dotagridpane. */
	@FXML
	private GridPane dotagridpane;

	/** The lolgridpane. Where lol data is shown */
	@FXML
	private GridPane lolgridpane;

	/** The tab 1 border pane. */
	@FXML
	private BorderPane tab1BorderPane;

	/** The user image. */
	@FXML
	private ImageView userImage;

	/** The username and email label. */
	@FXML
	private Label userNameLabel;
	
	/** The email label. */
	@FXML
	private Label emailLabel;

	/** The edit user info cancel button. */
	@FXML
	private Button chooseImageButton;
	
	/** The edit user info button. */
	@FXML
	private Button editUserInfoButton;
	
	/** The edit user info cancel button. */
	@FXML
	private Button editUserInfoCancelButton;
	
	/** The add cs button. */
	@FXML
	private Button addCsButton;
	
	/** The add dota button. */
	@FXML
	private Button addDotaButton;
	
	/** The add lol button. */
	@FXML
	private Button addLolButton;	

	/** The textfields for the user to input new infos. */
	@FXML
	private TextField newUsername;
	
	/** The new user email. */
	@FXML
	private TextField newUserEmail;

	/** Vbox for buttons where to choose which Dota match to load. */
	@FXML
	private VBox latestDotaMatches;

	/** Vbox for buttons where to choose which CsGo match to load. */
	@FXML
	private VBox latestCsgoMatches;

	/** The latest lol matches. */
	@FXML
	private VBox latestLolMatches;
	
	/** The choose language. */
	@FXML
	private ChoiceBox<String> chooseLanguage;

	/** The line chart kills. */
	@FXML
	private BarChart lineChartKills;
	
	/** The line chart KD. */
	@FXML
	private BarChart lineChartKD;

	/** The texts. */
	private ResourceBundle texts;
	/** Locale */
	private Locale currentLocale;
	

	/** The Api. */
	API Api = new API(this);

	/** The csgo icon. */
	// Icons
	Image csgoIcon = new Image(this.getClass().getResource("csgo_iconrotated.png").toString(), 80, 80, true, true);

	/** The dota icon. */
	Image dotaIcon = new Image(this.getClass().getResource("dota_logorotated.png").toString(), 80, 75, true, true);
	
	/** The lol icon. */
	Image lolIcon = new Image(this.getClass().getResource("league-of-legends_logo.png").toString(), 85, 80, true, true);

	/** The add game icon. */
	Image addGameIcon = new Image(this.getClass().getResource("png-white-plus-sign-3.png").toString(), 40, 40, true,
			true);

	/** The game sessions. */
	private ArrayList<GameSession> gameSessions = new ArrayList<>();

	/** The guifactory. Creates panes for stats and notes */
	private IGuiFactory guifactory = new GuiFactory();
	
	/**  Resources for gamesessions. */
	private GSessionResources gResources;
	
	/** The currently loaded ID. Which match is currently loaded */
	private int currentlyLoadedID;

	/** The user. */
	private User user;
	
	/** The oldlength. */
	private int oldlength;
	
	/** The gamesessioncontroller. */
	private GameSessionController gsc;
	
	/** The database. */
	private GameTrain db;

	/**
	 * Initialize the scene and localisation ChoiceBox.
	 *
	 * @param user the user
	 * @param locale locale that has been passed
	 */
	public void initialize(User user, Locale locale) { // Everything we want to be done before the UI loads
		
		
			Locale defaultLocale =locale;
			texts= ResourceBundle.getBundle("TextResources", locale);
			gResources = new GSessionResources(defaultLocale);
		
		
		this.user = user;
		//gResources = new GSessionResources(bundle);
		userNameLabel.setText(user.getUsername());
		emailLabel.setText(user.getEmail());

		chooseLanguage.setItems(FXCollections.observableArrayList("English", "Korea"));

		ImageView csgoview = new ImageView(csgoIcon);
		tab2.setGraphic(csgoview);
		tab2.setText(null);

		ImageView dotaview = new ImageView(dotaIcon);
		tab3.setGraphic(dotaview);
		tab3.setText(null);

		ImageView lolview = new ImageView(lolIcon);
		tab4.setGraphic(lolview);
		tab4.setText(null);

		ImageView addGameIconview = new ImageView(addGameIcon);
		tab5.setGraphic(addGameIconview);

		tabpane.setTabMinWidth(50);
		tabpane.setTabMinHeight(30);
		tabpane.setTabMaxWidth(50);
		tabpane.setTabMaxHeight(30);

		csTextAreaNotes.setWrapText(true);
		dotaTextAreaNotes.setWrapText(true);
		String matchID = "7236343"; // Demo/Testmatch
		// Api.readDotaMatchDetails(matchID);

		Image profilepic = new Image("https://users.metropolia.fi/~ninjalu/Gametrainicons/blobl.PNG", 150, 150, true,
				true);
		userImage.setImage(profilepic);
		chooseImageButton.setStyle(null);

		tabpane.getTabs().remove(tab2);
		tabpane.getTabs().remove(tab3);
		tabpane.getTabs().remove(tab4);

		chooseLanguage.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
				System.out.println(chooseLanguage.getItems().get((Integer) number2));

				if (chooseLanguage.getItems().get((Integer) number2).equals("Korea")) {
					ResourceBundle.clearCache();
					Locale locale = new Locale("ko", "KR");
					currentLocale = locale;

					ResourceBundle exampleBundle = ResourceBundle.getBundle("TextResources", locale);

					texts = exampleBundle;
					

					try {

						JMetro metroStyle = new JMetro(JMetro.Style.LIGHT);
						FXMLLoader fxmlLoader = new FXMLLoader();
						fxmlLoader.setLocation(getClass().getResource("Gametrain.fxml"));

						fxmlLoader.setResources(exampleBundle);
						Scene scene = new Scene(fxmlLoader.load(), 964, 600);
						GametrainController gtcontroller = fxmlLoader.getController();
						gtcontroller.initialize(user, locale);
						Stage stage = new Stage();
						stage.setTitle("Gametrain");
						stage.setScene(scene);
						metroStyle.applyTheme(scene);
						stage.show();

						// closes
						Stage stageToClose = (Stage) chooseLanguage.getScene().getWindow();
						stageToClose.hide();
					} catch (IOException e) {
						Logger logger = Logger.getLogger(getClass().getName());
						logger.log(Level.SEVERE, "Failed to create new Window.", e);
					}

					System.out.println(texts.toString());
				} else if (chooseLanguage.getItems().get((Integer) number2).equals("English")) {
					// ResourceBundle.clearCache();

					ResourceBundle.clearCache();
					Locale locale = new Locale("en", "GB");
					currentLocale = locale;

					ResourceBundle exampleBundle = ResourceBundle.getBundle("TextResources", locale);

					texts = exampleBundle;
					

					try {

						JMetro metroStyle = new JMetro(JMetro.Style.LIGHT);
						FXMLLoader fxmlLoader = new FXMLLoader();
						fxmlLoader.setLocation(getClass().getResource("Gametrain.fxml"));

						fxmlLoader.setResources(exampleBundle);
						Scene scene = new Scene(fxmlLoader.load(), 964, 600);
						GametrainController gtcontroller = fxmlLoader.getController();
						gtcontroller.initialize(user, locale);
						Stage stage = new Stage();
						stage.setTitle("Gametrain");
						stage.setScene(scene);
						metroStyle.applyTheme(scene);
						stage.show();

						// closes
						Stage stageToClose = (Stage) chooseLanguage.getScene().getWindow();
						stageToClose.hide();
					} catch (IOException e) {
						Logger logger = Logger.getLogger(getClass().getName());
						logger.log(Level.SEVERE, "Failed to create new Window.", e);
					}
					gResources = new GSessionResources(locale);
				}
				
			}	
		});
		gsc = new GameSessionController();
		db = new GameTrain();
	}


	/**
	 * Handle edit user info.
	 *
	 * @param event the event
	 */
	@FXML
	public void handleEditUserInfo(ActionEvent event) {

		if (editUserInfoButton.getText().compareTo(texts.getString("profileEdit")) == 0) { // If text is Edit set text
																							// to Save and show new
			// buttons/labels
			editUserInfoButton.setText(texts.getString("profileSave"));
			chooseImageButton.setVisible(true);
			editUserInfoCancelButton.setVisible(true);
			newUsername.setVisible(true);
			newUserEmail.setVisible(true);

		} else if (editUserInfoButton.getText().compareTo(texts.getString("profileSave")) == 0
				&& (newUsername.getText().equals("") == false || newUserEmail.getText().equals("") == false)) {
			// If button says Save and Textfields are not empty, update infos
			if (newUsername.getText().equals("")) {

			} else {
				userNameLabel.setText(newUsername.getText());
			}
			if (newUserEmail.getText().equals("")) {

			} else {
				emailLabel.setText(newUserEmail.getText());
			}
			newUsername.clear();
			newUserEmail.clear();

		} else { // If buttons says Save but textfields are empty hide editing labels
			chooseImageButton.setVisible(false);
			editUserInfoCancelButton.setVisible(false);
			newUsername.setVisible(false);
			newUserEmail.setVisible(false);
			editUserInfoButton.setText(texts.getString("profileEdit"));
		}

	}


	/**
	 * Handle cancel user edit info.
	 *
	 * @param event the event
	 */
	@FXML
	public void handleCancelUserEditInfo(ActionEvent event) {
		newUsername.clear();
		newUserEmail.clear();
		chooseImageButton.setVisible(false);
		editUserInfoCancelButton.setVisible(false);
		newUsername.setVisible(false);
		newUserEmail.setVisible(false);
		editUserInfoButton.setText(texts.getString("profileEdit"));
	}

	/**
	 * Handle save user accounts.
	 *
	 * @param event the event
	 */
	@FXML
	public void handleSaveUserAccounts(ActionEvent event) {

	}

	/**
	 * Handle choose image.
	 *
	 * @param event the event
	 */
	@FXML
	public void handleChooseImage(ActionEvent event) {

	}

	/**
	 * S Handle start cs sessionbutton.
	 *
	 * @param event the event
	 */
	// Event Listener on Button[#csbuttonStartsession].onAction
	@FXML
	public void handleStartCsSessionbutton(ActionEvent event) {
		gameSessions.add(createNewSession(1, null));
		csgogridpane.getChildren().clear();

		for (int i = 0; i < gResources.getStatsForGame(1).size(); i++) {

			TitledPane titledpane = guifactory.createTitledpane(gResources.getStatsForGame(1).get(i), "");
			if (i > 3) { // Change row when current row is filled
				csgogridpane.add(titledpane, i - 4, 1);
			} else {
				csgogridpane.add(titledpane, i, 0);
			}
		}
	}

	/**
	 * Handle start dota sessionbutton.
	 *
	 * @param event the event
	 */
	// Event Listener on Button[#dotabuttonStartsession].onAction
	@FXML
	public void handleStartDotaSessionbutton(ActionEvent event) {
		gameSessions.add(createNewSession(0, null));
		dotagridpane.getChildren().clear();

		for (int i = 0; i < gResources.getStatsForGame(0).size(); i++) {

			TitledPane titledpane = guifactory.createTitledpane(gResources.getStatsForGame(0).get(i), "");
			if (i > 3) { // Change row when current row is filled
				dotagridpane.add(titledpane, i - 4, 1);
			} else {
				dotagridpane.add(titledpane, i, 0);
			}
		}
	}


	/**
	 * Handle start lol sessionbutton.
	 *
	 * @param event the event
	 */
	@FXML
	public void handleStartLolSessionbutton(ActionEvent event) {
		gameSessions.add(createNewSession(2, null));
		lolgridpane.getChildren().clear();

		for (int i = 0; i < gResources.getStatsForGame(2).size(); i++) {

			TitledPane titledpane = guifactory.createTitledpane(gResources.getStatsForGame(2).get(i), "");
			if (i > 3) { // Change row when current row is filled
				lolgridpane.add(titledpane, i - 4, 1);
			} else {
				lolgridpane.add(titledpane, i, 0);
			}
		}
	}

	/**
	 * Handle save csnotesbutton.
	 *
	 * @param event the event
	 */
	// Event Listener on Button[#csButtonSavenotes].onAction
	@FXML
	public void handleSaveCsnotesbutton(ActionEvent event) {

		GameSession session = getSessionByID(currentlyLoadedID);
		System.out.print(csTextAreaNotes.getText());
		if (csTextAreaNotes.getText().trim().length() > 0 && session.getNotes().size() < 8) { // Check if a new comment
																								// has been written

			TitledPane newnote = guifactory.createNotepane(csTextAreaNotes.getText());
			session.addNote(csTextAreaNotes.getText());
			System.out.println(session.getNotes().toString());

			if (session.getNotes().size() <= 4) {
				csgogridpane.add(newnote, session.getNotes().size() - 1, 2);

			} else {
				csgogridpane.add(newnote, session.getNotes().size() - 5, 3);
			}

			csTextAreaNotes.clear();
		}

		ObservableList<Node> temp = csgogridpane.getChildren();
		ArrayList<String> newstats = new ArrayList<>();

		for (int i = 0; i < 8; i++) {

			TitledPane temppane = (TitledPane) temp.get(i);

			newstats.add(((TextArea) temppane.getContent()).getText());

		}
		ArrayList<String> newnotes = new ArrayList<>();
		if (session.getNotes().size() > 0) {

			for (int i = 9; i < session.getNotes().size() + 9; i++) {
				TitledPane temppane = (TitledPane) temp.get(i - 1);

				newnotes.add(((TextArea) temppane.getContent()).getText());
				System.out.println(newnotes.toString());
			}
		}
		System.out.println(newstats.toString() + temp.size());
		session.setStats(newstats);
		session.setNotes(newnotes);
		GameSessionController gsc = new GameSessionController();
		UserStats[] userstat = gsc.parseGameSessionToUserStats(session, user);
		if (gsc.getGSession(session.getDatabaseSessionID()) == null) {
			int sessionid = gsc.storeGSession(userstat[0]);
			userstat[0].getGsession().setSessionID(sessionid);
			if (userstat.length > 1) {
				for (int i = 1; i < userstat.length; i++) {
					gsc.createGSessionComment(userstat[0].getGsession(), userstat[i]);
				}
			}
			session.setDatabaseSessionID(sessionid);
			System.out.println("Session id : " + sessionid);
		} else {
			System.out.println("Session id kun päivitetään : " + session.getSessionID());
			userstat[0].getGsession().setSessionID(session.getDatabaseSessionID());
			gsc.updateGSession(userstat[0]);
			UserStats[] databasestats = gsc.getGSessionWithComments(session.getSessionID());
			System.out.println(databasestats.length);
			if (userstat.length > databasestats.length) {
				for (int i = databasestats.length; i < userstat.length; i++) {
					gsc.createGSessionComment(userstat[0].getGsession(), userstat[i]);
				}
			}
		}
		ChartDataController cdc = new ChartDataController(user);
		int gameid = db.readGameByName("CSGO").getGameID();
		lineChartKills.getData().remove(0);
		lineChartKills.getData().add(cdc.getAllSessionsSeries(gameid, "kills"));
		cdc = new ChartDataController(user);
		lineChartKD.getData().remove(0);
		lineChartKD.getData().add(cdc.getAllSessionsSeries(gameid, "stat4"));
	}


	/**
	 * Make a new note if user has written one, and save changes to other stats and
	 * notes.
	 *
	 * @param event the event
	 */
	@FXML
	public void handleSaveDotanotes(ActionEvent event) {
		GameSession session = getSessionByID(currentlyLoadedID);
		System.out.print(dotaTextAreaNotes.getText());
		if (dotaTextAreaNotes.getText().trim().length() > 0 && session.getNotes().size() < 8) { // Check if a new
																								// comment has been
																								// written

			TitledPane newnote = guifactory.createNotepane(dotaTextAreaNotes.getText()); // If a note has been written
																							// make a pane for it
			session.addNote(dotaTextAreaNotes.getText());
			System.out.println(session.getNotes().toString());

			if (session.getNotes().size() <= 4) { // Find the next spot in the gridpane for the note
				dotagridpane.add(newnote, session.getNotes().size() - 1, 2);
			} else {
				dotagridpane.add(newnote, session.getNotes().size() - 5, 3);
			}

			dotaTextAreaNotes.clear(); // Empty the text area
		}
		// Then read the stats in case of changes
		ObservableList<Node> temp = dotagridpane.getChildren();
		ArrayList<String> newstats = new ArrayList<>();

		for (int i = 0; i < 8; i++) {
			TitledPane temppane = (TitledPane) temp.get(i);
			newstats.add(((TextArea) temppane.getContent()).getText());
		}
		ArrayList<String> newnotes = new ArrayList<>(); // Read notes in case of changes
		if (session.getNotes().size() > 0) {
			for (int i = 9; i < session.getNotes().size() + 9; i++) {
				TitledPane temppane = (TitledPane) temp.get(i - 1);

				newnotes.add(((TextArea) temppane.getContent()).getText());
				System.out.println(newnotes.toString());
			}
		}
		System.out.println(newstats.toString() + temp.size());
		session.setStats(newstats); // Save new stats to the session
		session.setNotes(newnotes); // Save new noted to the session
	}


	/**
	 * Handle save lolnotes.
	 *
	 * @param event the event
	 */
	@FXML
	public void handleSaveLolnotes(ActionEvent event) {
		GameSession session = getSessionByID(currentlyLoadedID);
		System.out.print(lolTextAreaNotes.getText());
		if (lolTextAreaNotes.getText().trim().length() > 0 && session.getNotes().size() < 8) { // Check if a new comment
																								// has been written

			TitledPane newnote = guifactory.createNotepane(lolTextAreaNotes.getText());
			session.addNote(lolTextAreaNotes.getText());
			System.out.println(session.getNotes().toString());

			if (session.getNotes().size() <= 4) {
				lolgridpane.add(newnote, session.getNotes().size() - 1, 2);

			} else {
				lolgridpane.add(newnote, session.getNotes().size() - 5, 3);
			}

			lolTextAreaNotes.clear();
		}

		ObservableList<Node> temp = lolgridpane.getChildren();
		ArrayList<String> newstats = new ArrayList<>();

		for (int i = 0; i < 8; i++) {

			TitledPane temppane = (TitledPane) temp.get(i);

			newstats.add(((TextArea) temppane.getContent()).getText());

		}
		ArrayList<String> newnotes = new ArrayList<>();
		if (session.getNotes().size() > 0) {

			for (int i = 9; i < session.getNotes().size() + 9; i++) {
				TitledPane temppane = (TitledPane) temp.get(i - 1);

				newnotes.add(((TextArea) temppane.getContent()).getText());
				System.out.println(newnotes.toString());
			}
		}
		System.out.println(newstats.toString() + temp.size());
		session.setStats(newstats);
		session.setNotes(newnotes);
	}


	/**
	 * Handle delete cs session button.
	 *
	 * @param event the event
	 */
	@FXML
	public void handleDeleteCsSessionButton(ActionEvent event) {
		System.out.println(currentlyLoadedID);
		GameSession session = getSessionByID(currentlyLoadedID);
		System.out.println(session.getSessionID());

		ArrayList<GameSession> sessions = getSessionsForGame(1);

		latestCsgoMatches.getChildren().remove(sessions.indexOf(session));
		gameSessions.remove(session);
		gsc.deleteGSession(session.getDatabaseSessionID());

		currentlyLoadedID = -1;
		csgogridpane.getChildren().clear();
	}


	/**
	 * Handle end dota sessionbutton.
	 *
	 * @param event the event
	 */
	@FXML
	public void handleDeleteDotaSessionButton(ActionEvent event) {
		GameSession session = getSessionByID(currentlyLoadedID);
		System.out.println(session.getSessionID());

		ArrayList<GameSession> sessions = getSessionsForGame(0);

		latestDotaMatches.getChildren().remove(sessions.indexOf(session));
		gameSessions.remove(session);

		currentlyLoadedID = -1;
		dotagridpane.getChildren().clear();
	}

	/**
	 * Handledelete lol session button.
	 *
	 * @param event the event
	 */
	@FXML
	public void handledeleteLolSessionButton(ActionEvent event) {
		GameSession session = getSessionByID(currentlyLoadedID);
		System.out.println(session.getSessionID());

		ArrayList<GameSession> sessions = getSessionsForGame(2);

		latestLolMatches.getChildren().remove(sessions.indexOf(session));
		gameSessions.remove(session);

		currentlyLoadedID = -1;
		lolgridpane.getChildren().clear();
	}

	/**
	 * Handle end cs sessionbutton.
	 *
	 * @param event the event
	 */
	// Event Listener on Button[#csbuttonEndSession].onAction
	@FXML
	public void handleEndCsSessionbutton(ActionEvent event) {
		// TODO Autogenerated
	}

	/**
	 * Handle end dota session button.
	 *
	 * @param event the event
	 */
	@FXML
	public void handleEndDotaSessionButton(ActionEvent event) {
		// TODO Autogenerated
	}

	/**
	 * Handle end lol sessionbutton.
	 *
	 * @param event the event
	 */
	@FXML
	public void handleEndLolSessionbutton(ActionEvent event) {

	}

	/**
	 * Load game sessions.
	 */
	public void loadGameSessions() { // Get player sessions from database
		// get all the gamesessions of one user on one arraylist
	}

	/**
	 * Gets the sessions for specified game.
	 *
	 * @param gameID the game ID
	 * @return the sessions for game
	 */
	public ArrayList<GameSession> getSessionsForGame(int gameID) { // return arraylist of games that match one game ID
		ArrayList<GameSession> sessions = new ArrayList<>();

		for (GameSession x : gameSessions) {
			if (x.getGameID() == gameID) {
				sessions.add(x);
			}
		}

		return sessions;

	}

	/**
	 * Gets the session by ID.
	 *
	 * @param sessionID the session ID
	 * @return the session by ID
	 */
	public GameSession getSessionByID(int sessionID) {

		for (GameSession x : gameSessions) {
			if (x.getSessionID() == sessionID) {
				return x;
			}
		}

		return null;
	}

	/**
	 * Creates a new session and adds a button for the session.
	 *
	 * @param GameID the game ID
	 * @param session the session
	 * @return the boolean
	 */
	public GameSession createNewSession(int GameID, GameSession session) { // Create a new session with empty panes
		GridPane gridpane = getselectedpane();
		gridpane.getChildren().clear();

		GameSession newSession = new GameSession(GameID);
		ArrayList<String> stats = new ArrayList<>(); // Create an empty Arraylist for the empty Gamesession

		if (session != null) {
			newSession.setStats(session.getStats());
			newSession.setDatabaseSessionID(session.getDatabaseSessionID());
		} else {
			newSession.setStats(stats);
		}
		Button newbutton = new Button();
		int buttonnr = newSession.getSessionID();

		String asd = LocalDateTime.now().toString();
		asd = asd.substring(0, 16);
		asd = asd.replace('T', ' ');

		newbutton.setText(texts.getString("Session") + newSession.getSessionID() + " " + asd);

		if (GameID == 0) {
			newbutton.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent e) {

					loadSessionData(newSession.getSessionID(), GameID);
				}
			});
			latestDotaMatches.getChildren().add(newbutton);
			currentlyLoadedID = newSession.getSessionID();
		} else if (GameID == 1) {
			newbutton.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent e) {

					loadSessionData(newSession.getSessionID(), GameID);
				}
			});
			latestCsgoMatches.getChildren().add(newbutton);
			currentlyLoadedID = newSession.getSessionID();
		} else if (GameID == 2) {
			newbutton.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent e) {

					loadSessionData(newSession.getSessionID(), GameID);
				}
			});
			latestLolMatches.getChildren().add(newbutton);
			currentlyLoadedID = newSession.getSessionID();
		}
		return newSession;
	}

	
	/**
	 * Handle cs refresh button.
	 *
	 * @param event the event
	 */
	@FXML
	public void handlecsButtonRefresh(ActionEvent event) {
		System.out.println("Refreshing...");
		InterfaceAPI csgoapi = new CsgoAPI("76561198061586349", user);
		if (csgoapi.isThereNewMatch()) {
			GameSessionController gsc = new GameSessionController();
			UserStats[] apiuserstats = { csgoapi.changeAPIDataToUserStats() };
			gsc.storeGSession(apiuserstats[0]);
			GameSession session = gsc.parseUserStatsToGameSession(apiuserstats);
			gameSessions.add(createNewSession(1, session));
			csgogridpane.getChildren().clear();

			for (int a = 0; a < gResources.getStatsForGame(1).size(); a++) {

				TitledPane titledpane = guifactory.createTitledpane(gResources.getStatsForGame(1).get(a),
						session.getStats().get(a));
				if (a > 3) { // Change row when current row is filled
					csgogridpane.add(titledpane, a - 4, 1);
				} else {
					csgogridpane.add(titledpane, a, 0);
				}
			}
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setContentText("Match found");
			alert.showAndWait().ifPresent(response -> {
				if (response == ButtonType.OK) {

				}
			});
		}
		ChartDataController cdc = new ChartDataController(user);
		int gameid = db.readGameByName("CSGO").getGameID();
		lineChartKills.getData().remove(0);
		lineChartKills.getData().add(cdc.getAllSessionsSeries(gameid, "kills"));
		cdc = new ChartDataController(user);
		lineChartKD.getData().remove(0);
		lineChartKD.getData().add(cdc.getAllSessionsSeries(gameid, "stat4"));
	}

	
	/**
	 * Handle the lol refresh button
	 *
	 * @param event the event
	 */
	@FXML
	public void handlelolButtonRefresh(ActionEvent event) {
		System.out.println("Refreshing...");
		if (true) {
			 Alert alert = new Alert(AlertType.INFORMATION);
			 alert.setContentText("Match found");				
			alert.showAndWait().ifPresent(response -> {			
			     if (response == ButtonType.OK) {
			        
			     }
			 });
		}
	}
	
	/**
	 * Handle the dota refresh button
	 *
	 * @param event the event
	 */
	@FXML
	public void handledotaButtonRefresh(ActionEvent event) {
		System.out.println("Refreshing...");
		if (true) {
			 Alert alert = new Alert(AlertType.INFORMATION);
			 alert.setContentText("Match found");				
			alert.showAndWait().ifPresent(response -> {			
			     if (response == ButtonType.OK) {
			        
			     }
			 });
		}
	}
	
	/**
	 * Handle add cs button.
	 *
	 * @param event the event
	 */
	@FXML
	public void handleaddCsButton(ActionEvent event) {
		tabpane.getTabs().add(1, tab2);
		tab2.setStyle("-fx-rotate: 90");
		tab2.setStyle("-fx-padding: 2.55em");
		tabpane.getSelectionModel().select(tab2);
		GSession[] gsessions = gsc.getAllUsersGameSessions(user.getUserID(), db.readGameByName("CSGO").getGameID());
		for (int i = 0; i < gsessions.length; i++) {
			UserStats[] userstatsArray = { gsc.getGSession(gsessions[i].getSessionID()) };
			GameSession session = gsc.parseUserStatsToGameSession(userstatsArray);

			gameSessions.add(createNewSession(1, session));
			csgogridpane.getChildren().clear();

			for (int a = 0; a < gResources.getStatsForGame(1).size(); a++) {

				TitledPane titledpane = guifactory.createTitledpane(gResources.getStatsForGame(1).get(a),
						session.getStats().get(a));
				if (a > 3) { // Change row when current row is filled
					csgogridpane.add(titledpane, a - 4, 1);
				} else {
					csgogridpane.add(titledpane, a, 0);
				}
			}
		}
		ChartDataController cdc = new ChartDataController(user);
		int gameid = db.readGameByName("CSGO").getGameID();

		lineChartKills.getData().add(cdc.getAllSessionsSeries(gameid, "kills"));
		cdc = new ChartDataController(user);
		lineChartKD.getData().add(cdc.getAllSessionsSeries(gameid, "stat4"));

	}

	
	/**
	 * Handle add lol button.
	 *
	 * @param event the event
	 */
	@FXML
	public void handleaddLolButton(ActionEvent event) {
		tabpane.getTabs().add(1, tab4);
		tab4.setStyle("-fx-rotate: 90");
		tab4.setStyle("-fx-padding: 2.55em");
		tabpane.getSelectionModel().select(tab4);
	}
	
	/**
	 * Handle add dota button.
	 *
	 * @param event the event
	 */
	@FXML
	public void handleaddDotaButton(ActionEvent event) {
		tabpane.getTabs().add(1, tab3);
		tab3.setStyle("-fx-rotate: 90");
		tab3.setStyle("-fx-padding: 2.55em");
		tabpane.getSelectionModel().select(tab3);
	}
	
	
	
	
	/**
	 * Load session data.
	 *
	 * @param sessionID the session ID
	 * @param GameID the game ID
	 */
	protected void loadSessionData(int sessionID, int GameID) { // sessionIndex can be changed to gamesession, at
		GameSession tobeloaded = getSessionByID(sessionID); // latest when there is data in db
		GridPane gridpane = getselectedpane();
		gridpane.getChildren().clear(); // Clear whole gridpane
		ArrayList<String> statTitle = gResources.getStatsForGame(GameID);
		ArrayList<String> sessionstats = tobeloaded.getStats();

		if (tobeloaded.getStats().isEmpty()) {
			for (int i = 0; i < statTitle.size(); i++) {
				sessionstats.add("");

			}
		}

		for (int i = 0; i < statTitle.size(); i++) { // Make titledpanes for the stats

			TextArea statText = new TextArea(sessionstats.get(i));
			TitledPane titledpane = guifactory.createTitledpane(statTitle.get(i), statText.getText());

			if (i > 3) { // Change row when current row is filled
				gridpane.add(titledpane, i - 4, 1);
			} else {
				gridpane.add(titledpane, i, 0);
			}
			currentlyLoadedID = tobeloaded.getSessionID();
		}
		if (tobeloaded.getNotes().isEmpty()) { // If the session has no notes, make sure the
												// note row
			// is empty
		} else { // Load notes
			for (int i1 = 0; i1 < tobeloaded.getNotes().size(); i1++) {
				// System.out.println(dotaPlayerSessions.get(currentlyLoadedID).getNotes().size());
				gridpane.add(guifactory.createNotepane(tobeloaded.getNotes().get(i1)), i1, 2);
			}
		}
		currentlyLoadedID = tobeloaded.getSessionID();
		System.out.println("Currently loaded session: ID:" + currentlyLoadedID);

	}

	

	/**
	 * Gets the currently selected pane.
	 *
	 * @return the selectedpane
	 */
	protected GridPane getselectedpane() { 
	
		ArrayList<Tab> tabs = new ArrayList<>(Arrays.asList(tab1, tab2, tab3, tab4));
		Tab selectedTab = null;

		for (Tab tab : tabs) {

			if (tab.isSelected() == true) {
				selectedTab = tab;
			}
		}

		AnchorPane anc = null;
		ScrollPane scroll = null;
		SplitPane split = null;
		TabPane tabpane = null;
		AnchorPane anc2 = null;
		GridPane gridpane;
		anc = (AnchorPane) selectedTab.getContent();
		scroll = (ScrollPane) anc.getChildren().get(6);
		split = (SplitPane) scroll.getContent();
		tabpane = (TabPane) split.getItems().get(1);
		Tab tab = tabpane.getTabs().get(0);
		anc2 = (AnchorPane) tab.getContent();
		gridpane = (GridPane) anc2.getChildren().get(0);
		return gridpane;
	}


	/**
	 * Gets the graphpane from the currently selected tab.
	 *
	 * @return the graphpaneselectedpane
	 */
	protected AnchorPane getGraphpaneselectedpane() { // Get the Anchorpane for Graphs of the currently selected game

		ArrayList<Tab> tabs = new ArrayList<>(Arrays.asList(tab1, tab2, tab3, tab4));
		Tab selectedTab = null;

		for (Tab tab : tabs) {

			if (tab.isSelected() == true) {
				selectedTab = tab;
			}
		}

		AnchorPane anc = null;
		ScrollPane scroll = null;
		SplitPane split = null;
		TabPane tabpane = null;
		AnchorPane anc2 = null;
		anc = (AnchorPane) selectedTab.getContent();
		scroll = (ScrollPane) anc.getChildren().get(4); // Muuta 4->5 jos ei toimi
		split = (SplitPane) scroll.getContent();
		tabpane = (TabPane) split.getItems().get(1);
		Tab tab = tabpane.getTabs().get(1);
		anc2 = (AnchorPane) tab.getContent();
		return anc2;
	}
}
