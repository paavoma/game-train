package view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.ResourceBundle;

/**@author Ninja
 * The Class GSessionResources. Has text resources for the currently implemented games.
 */
public class GSessionResources {
	/** The current locale. */
	private Locale currentLocale;
	/** The resource texts. */
	private ResourceBundle texts;
	
	// Dota 2
	ArrayList<String> apistats;
	/** The tracked dota stats. */
	ArrayList<String> trackedDotaStats;
	/** The tracked cs stats. */
	private ArrayList<String> trackedCsStats; 
	/** The tracked lol stats. */
	private ArrayList<String> trackedLolStats;
	
	public GSessionResources (Locale locale) {
		this.currentLocale=locale;
		texts = ResourceBundle.getBundle("TextResources", currentLocale);
		

	
	/** The stats in the format the api wants them. Dota 2 */
	apistats = new ArrayList<>(Arrays.asList("radiant_win", "kills", "deaths", "assists", "denies", "last_hits", "gold_per_min", "xp_per_min"));
	

	trackedDotaStats = new ArrayList<>(Arrays.asList(
			texts.getString("statWin"),
			texts.getString("statKills"),
			texts.getString("statDeaths"),
			texts.getString("statAssists"),
			texts.getString("statDenies"),
			texts.getString("statLastHits"),
			texts.getString("statGoldPerMin"),
			texts.getString("statXPPerMin")));
	
	trackedCsStats = new ArrayList<>(Arrays.asList(
			texts.getString("statWin"),
			texts.getString("statKills"),
			texts.getString("statDeaths"),
			texts.getString("statAssists"),
			texts.getString("statDmgDone"),
			texts.getString("statLastHits"),
			texts.getString("statGoldPerMin"),
			texts.getString("statXPPerMin")));
	
	trackedLolStats = new ArrayList<>(Arrays.asList(
			texts.getString("statWin"),
			texts.getString("statKills"),
			texts.getString("statDeaths"),
			texts.getString("statAssists"),
			texts.getString("statDenies"),
			texts.getString("statLastHits"),
			texts.getString("statGoldPerMin"),
			texts.getString("statXPPerMin")));
}
	/**
	 * Gets the apistats.
	 *
	 * @return the apistats
	 */
	public ArrayList<String> getApistats() {
		return apistats;
	}

	/**
	 * Gets the trackedstats.
	 *
	 * @return the trackedstats
	 */
	public ArrayList<String> getTrackedstats() {
		return trackedDotaStats;
	}

	/**
	 * Gets the stat names for a game.
	 *
	 * @param gameId the game id
	 * @return the stats for game
	 */
	public ArrayList<String> getStatsForGame(int gameId){
		switch(gameId) {
		  case 0:						// Dota
				return trackedDotaStats;

		  case 1:						// CSGO
			  	return trackedCsStats;

		  case 2: 
			  return trackedLolStats;
		  default:
			  return trackedDotaStats;
		}
		
	}

	
}
