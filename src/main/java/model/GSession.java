package model;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;
/**
 * This class is GSession, and it's used to make GAMESESSION table to database.
 * Also GSession stores all variables that are needed to know which UserStats row GSession is pointing at.
 * And GSession is one parameter of UserStats.
 * @author Petteri Pipponen
 *
 */
@Entity
@Table(name="GAMESESSION")
public class GSession {
	
	

	@Id
	@GeneratedValue
	@Column(name="sessionID")
	private int sessionID;
	
	@Column(name="sessionName")
	private String sessionName;
	
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "startTime", columnDefinition="DATETIME", nullable = false)
	private Date startTime;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "endTime", columnDefinition="DATETIME")
	private Date endTime;
	
	@ManyToOne
	@JoinColumn(name="USER")
	private User user;
	
	@ManyToOne
	@JoinColumn(name="GAMES")
	private Games games;
	
	public GSession() {
	}
	
	public GSession(int sessionID, String sessionName, Date startTime, Date endTime, User user, Games games) {
		super();
		this.sessionID = sessionID;
		this.sessionName = sessionName;
		this.startTime = startTime;
		this.endTime = endTime;
		this.user = user;
		this.games = games;
	}
	/**
	 * Method is used to get GSessions sessionID witch is integer variable.
	 * @return sessionID - Integer value.
	 */
	public int getSessionID() {
		return sessionID;
	}
	/**
	 * Method is used to set GSessions integer value for variable sessionID.
	 * @param sessionID - integer variable. 
	 */
	public void setSessionID(int sessionID) {
		this.sessionID = sessionID;
	}
	/**
	 * Method is used to get GSessions sessionName.
	 * @return sessionName - String variable.
	 */
	public String getSessionName() {
		return sessionName;
	}
	/**
	 * Method is used to set GSessions sessionName variable.
	 * @param sessionName - String variable.
	 */
	public void setSessionName(String sessionName) {
		this.sessionName = sessionName;
	}
	/**
	 * Method is used to get GSessions startTime.
	 * @return startTime - Date object.
	 */
	public Date getStartTime() {
		return startTime;
	}
	/**
	 * Method is used to set GSessions startTime.
	 * @param startTime - Date Object.
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	/**
	 * Method is used to get GSessions endTime.
	 * @return endTime - Date Object.
	 */
	public Date getEndTime() {
		return endTime;
	}
	/**
	 * Method is used to set GSessions endTime.
	 * @param endTime - Date Object
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	/**
	 * Method is used to get GSessions user.
	 * @return user - User Object who owns GSession.
	 */
	public User getUser() {
		return user;
	}
	/**
	 * Method is used to set User for GSession.
	 * @param user - User Object who owns GSession.
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
	/**
	 * Method is used to get GSessions game. 
	 * @return games - Games Object.
	 */
	public Games getGames() {
		return games;
	}
	/**
	 * Method is used to set Games (Game) for GSession. 
	 * @param games - Games Object.
	 */
	public void setGames(Games games) {
		this.games = games;
	}
	
}
