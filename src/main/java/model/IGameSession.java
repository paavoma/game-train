package model;

import java.util.ArrayList;

public interface IGameSession {

	static int count = 0;
	int sessionID = 0;

	ArrayList<String> stats = new ArrayList<>(); // Actual stat number/data
	ArrayList<String> notes = new ArrayList<>(); // Notes as Strings
	
	public String getGametitle();

	public void setGametitle(String gametitle);

	public ArrayList<String> getStats();

	public void setStats(ArrayList<String> stats);

	public void addNote(String note);

	public void setNotes(ArrayList<String> notes);

	public ArrayList<String> getNotes();

	public int getSessionID();
}
