package model;

import javafx.scene.control.*;

/**@author Ninja
 * A factory for creating IGui objects.
 */
public interface IGuiFactory {

	
	/**
	 * Creates a pane for notes.
	 *
	 * @param content the content
	 * @return the titled pane
	 */
	public TitledPane createNotepane(String content);
	
	/**
	 *  Creates a pane for the Sessiondata
	 *
	 * @param title the title for a titleplane
	 * @param content the content for the titlepane
	 * @return the titled pane
	 */
	public TitledPane createTitledpane(String title, String content);
	
	/**
	 * Creates a new Button for a session. Not implemented.
	 *
	 * @param SessionID the session ID
	 * @param GameID the game ID
	 * @return the button
	 */
	public Button createSessionButton(String SessionID, int GameID);
}
