package model;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import controller.GameSessionController;
import view.GametrainController;

/**
 * This class connects with Steam Web API. 
 * It contains methods that are used to make calls to get Users CSGO video game stats.
 * @author Eelis Melto
 *
 */
public class CsgoAPI implements InterfaceAPI{
	
	/** The csplayer ID. */
	private String csplayerID = "76561198061586349";
	
	/** The user */
	private User user;
	
	/** The user ID */
	private int userID;
	
	/** The game csgo*/
	private Games csgo;
	
	/** The game ID */
	private int gameID;
	
	/** The my steamapikey. */
	private static String mySteamapikey = "BA8AA3AF84BF8E42D97BF8AE50500121";

	/** The json string. */
	private String jsonString = "";
	
	private JSONArray jsonArray;
	
	/** The Array that contains latest match detail names for api**/
	private String[] latestMatchDataNames = {"last_match_t_wins","last_match_ct_wins","last_match_wins"
	                                         ,"last_match_kills","last_match_deaths","last_match_mvps"
	                                         ,"last_match_damage","last_match_money_spent","last_match_contribution_score"};
	
	/** The Array that contains overall stat detail names for api**/
	private String[] overallMatchDataNames = {"total_kills","total_deaths","total_time_played","total_planted_bombs"
											  ,"total_defused_bombs","total_wins","total_damage_done"};
	/** The Array that contains players latestmatch statistics from Steam Web API */
	public List<Integer> mLatestMatch;
	/** The Array that contains players overall statistics Steam Web API */
	public List<Integer> mOverallData;
	/** The database connection variable */
	private GameTrain db;
	
	/**
	 * Constructor that constructs CsgoAPI class. It needs two parameters.
	 * @param csPlayerID - String that contains Users steam id. 
	 * @param user - User object that view passes.
	 */
	public CsgoAPI(String csPlayerID, User user) {
		this.db = new GameTrain();
		this.csplayerID = csPlayerID;
		this.user = user;
		this.userID = user.getUserID();
		this.csgo = db.readGameByName("CSGO");
		this.gameID = csgo.getGameID();
		mLatestMatch = new ArrayList<>();
		mOverallData = new ArrayList<>();
	}
	
	/**
	 * Used to read CSGO API. This method is private because its only used on this class other methods.
	 * Gets CSGO API data as jsonString and then formats it to jsonArray.
	 * @param request - request
	 * @param subrequest - subrequest
	 * @param parameter - parameter
	 */
	@Override
	public void readAPI(String request, String subrequest, String parameter) {
		try {
			// example
			// "https://api.steampowered.com/IDOTA2Match_570/GetMatchDetails/v1?key="+mySteamapikey+"&match_id="+matchID)
			URL url = new URL("https://api.steampowered.com/" + request + "/" + subrequest + "?appid=730&key=" + mySteamapikey
					+ "&" + parameter);
			System.out.println(url);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;

			while ((output = br.readLine()) != null) {
				jsonString += output;
				// System.out.println(output);

			}
			JSONObject jsonObj = new JSONObject(jsonString);
			jsonArray = jsonObj.getJSONObject("playerstats").getJSONArray("stats");
			System.out.println(jsonString);
			conn.disconnect();
		} catch (Exception e) {
			System.out.println("Exception in API, readMatchInfo:- " + e);
		}

	}
	/**
	 * This method reads users latest CSGO match data from CSGO API.
	 * @return latestMatch - its List Integer containing latest match data.
	 * 
	 */
	@Override
	public List<Integer> readLatestMatchData() {
		String request = "ISteamUserStats";
		String subrequest = "GetUserStatsForGame/v2/";
		String parameter = "steamid=" + csplayerID;
		readAPI(request, subrequest, parameter);
		List<Integer> latestMatch = new ArrayList<>();
		for (String string : latestMatchDataNames) {
			latestMatch.add(getMatchDetailWithItem(string));
		};
		return latestMatch;
	}
	/**
	 * This method reads all Users overall CSGO statistic from Steam Web API
	 * using method readAPi. Then method uses getMatchDataWithItem to parse 
	 * @return overallData - List object containing all overall csgo statistic for user.
	 * 
	 */
	@Override
	public List<Integer> readAllOverallMatchesData() {
		String request = "ISteamUserStats";
		String subrequest = "GetUserStatsForGame/v2/";
		String parameter = "steamid=" + csplayerID;
		readAPI(request, subrequest, parameter);
		List<Integer> overallData = new ArrayList<>();
		for (String string : overallMatchDataNames) {
			overallData.add(getMatchDetailWithItem(string));
		}
		return overallData;
	}
	
	/**
	 * This method is used to get information is new match available in csgo api.
	 * @return boolean - true or false, if true there is new match available and else there is not new game available.
	 */
	@Override
	public boolean isThereNewMatch() {
		if (this.mLatestMatch.isEmpty()) {
			GameSessionController gtc = new GameSessionController();
			UserStats session = gtc.getLatestGSession(userID, gameID);
			if (session == null) { //If users CSGO sessions are empty
				this.mLatestMatch = this.readLatestMatchData();
				return true;				
			}
			List<Integer> newLatestMatch = this.readLatestMatchData();
			//comparing if database latest match is same than CSGO APIs latest match.
			//IF (db.kills == api.kills && db.deaths == api.deaths && db.mvps == api.mvps && db.damagedone == api.damagedone 
			// && db.moneyspent == api.moneyspent && db.score == api.score
			if (session.getKills() == newLatestMatch.get(3) && session.getDeaths() == newLatestMatch.get(4) && session.getStat3() == newLatestMatch.get(5)
					&& session.getStat4() == newLatestMatch.get(6) && session.getStat5() == newLatestMatch.get(7) && session.getStat6() == newLatestMatch.get(8)) {
				this.mLatestMatch = newLatestMatch;
				return false;
			} else {
				this.mLatestMatch = newLatestMatch;
				return true;
			}
		} else {
			List<Integer> newLatestMatch = this.readLatestMatchData();
			if (newLatestMatch.equals(mLatestMatch)) 
			{
				return false;
			} else {
				this.mLatestMatch = newLatestMatch;
				return true;
			}
		}
	}
	/**
	 *  Method that is used to parse List mLatesMatch values to UserStats object.
	 *  @return csgomatch - UserStats that contains in its variables all values that list has.
	 *  @see UserStats for what userstats variables mean for csgo.
	 */
	@Override
	public UserStats changeAPIDataToUserStats() {
		UserStats csgomatch = new UserStats();
		GSession gs = new GSession(0, "", null, null, user, csgo);
		csgomatch.setKills(mLatestMatch.get(3));
		csgomatch.setDeaths(mLatestMatch.get(4));
		csgomatch.setStat1(mLatestMatch.get(2));
		if (csgomatch.getStat1() == 16) {
			csgomatch.setWin(1);
		}
		if(mLatestMatch.get(0) - 1 == mLatestMatch.get(2) ) {
			csgomatch.setStat2(mLatestMatch.get(1));
		}
		else if (mLatestMatch.get(1) - 1 == mLatestMatch.get(2)) {
			csgomatch.setStat2(mLatestMatch.get(0));
		}
		else if(mLatestMatch.get(1) == mLatestMatch.get(0)) {
			csgomatch.setStat2(15);
		}
		csgomatch.setStat3(mLatestMatch.get(5));
		csgomatch.setStat4(mLatestMatch.get(6));
		csgomatch.setStat5(mLatestMatch.get(7));
		csgomatch.setStat6(mLatestMatch.get(8));
		csgomatch.setUserStatsID(0);
		csgomatch.setGsession(gs);
		return csgomatch;
	}
	
	/**
	 * This method is used to get correct item value from jsonArray.
	 * @param item - String that contains name of the wanted value.
	 * @return stat - int value that had same name as @param item
	 */
	@Override
	public int getMatchDetailWithItem(String item) {
		//JSONObject jsonObj = new JSONObject(jsonString);
		
		int stat = 0;
		try {
			for (int i = 0; i < jsonArray.length(); i++)
		    {
		        JSONObject jitem = jsonArray.getJSONObject(i);
		        
		        if (jitem.getString("name").equals(item))
		        {
		        	
		            stat = jitem.getInt("value");
		            return stat;
		        }
		    }
			//stat = jsonObj.getJSONObject("playerstats").getJSONArray("stats").toString();
		} catch (Exception e) {
			
		}
		return stat;
		
	}
}
