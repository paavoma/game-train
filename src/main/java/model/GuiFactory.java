package model;

import java.util.Locale;
import java.util.ResourceBundle;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TitledPane;
import javafx.scene.text.Font;

/**@author Ninja
 * A factory for creating Gui objects. 
 */
public class GuiFactory implements IGuiFactory{
	
	/** The current locale. */
	private Locale currentLocale = new Locale("en", "GB", "UTF-8");
	
	/** The texts. */
	private ResourceBundle texts = ResourceBundle.getBundle("TextResources");
	
	/* 
	 * Creates a pane for notes.
	 */
	public TitledPane createNotepane(String content) {
		TextArea noteText = new TextArea(content);
		noteText.setMaxSize(200, 180);

		noteText.setWrapText(true);
		TitledPane titledpane = new TitledPane("Note", noteText);
		titledpane.setCollapsible(false);
		titledpane.setPadding(new Insets(10, 5, 0, 5));
		return titledpane;
	}
	
	/* 
	 * Creates a pane for the Sessiondata
	 */
	public TitledPane createTitledpane(String title, String content) {
		TextArea noteText = new TextArea(content);
		noteText.setMaxSize(200, 180);
		noteText.setFont(Font.font("Verdana", 26));
		noteText.setEditable(true);
		noteText.setWrapText(true);
		TitledPane titledpane = new TitledPane(title, noteText);
		titledpane.setCollapsible(false);
		titledpane.setPadding(new Insets(15, 5, 5, 5));
		return titledpane;
	}
	
	/*
	 * @see model.IGuiFactory#createSessionButton(java.lang.String, int)
	 */
	/* 
	 *  Might be implemented later
	 */
	public Button createSessionButton(String SessionID, int GameID) {
		return null;

	}
}
