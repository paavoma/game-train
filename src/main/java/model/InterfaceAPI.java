package model;

import java.util.List;
/**
 * This class is interface for CsGoAPI.
 * @author Eelis Melto
 *
 */
public interface InterfaceAPI {
	public void readAPI(String request, String subrequest, String parameter);
	public int getMatchDetailWithItem(String item);
	public List<Integer> readLatestMatchData();
	public List<Integer> readAllOverallMatchesData();
	public boolean isThereNewMatch();
	public UserStats changeAPIDataToUserStats();
}
