package model;

/**
 * @author Petteri Piipponen
 * Class for riot/league of legends API-queries
 * */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import controller.GameSessionController;
import model.UserStats;

public class LolAPI implements ILolAPI {

	/** The LOL summoner name you log in with */
	private String summonerName = "herrakersnantti";
	/** The LOL summoner name you log in with */
	private String region = "euw1";
	/** The LOL apikey */
	private String lolapikey = "RGAPI-7361f536-6a19-496f-b8a1-cb45b29066e2";
	
	
	private int userID;
	private int gameID;
		
	/** The Array that contains basic userdata like different id:s etc */
	private String[] lolUserDataNames = {"profileIconId", "name", "puuid",
			"summonerLevel", "accountId", "id", "revisionDate"};
	
	private String lastMatchId;
	
	private List<String> userData;
	private List<String> currentmatchData;
	private List<String> latestmatch;
	
	
	
	
 	/**
 	 *Constructor for LolApi class, summonerName is the user logs in to lol
 	 *@param region: region where account is located 
 	 *regions are: RU, KR, BR1, OC1, JP1, NA1, EUN1, EUW1, TR1, LA1, LA2
 	 *@param userID refers to the ID used in this app
 	 *@param gameID is the games ID in the database
 	 *@param summonerName in user logs
 	 */
	
	public LolAPI(String summonerName, String region, int userID, int gameID) {
		this.summonerName = summonerName;
		this.region = region;
		this.userID = userID;
		this.gameID = gameID;
		userData = new ArrayList<String>();
		latestmatch = new ArrayList<String>();
	}

	/** Method for reading the lol api, is used in other methods
	 *  that give the parameters to this method. Refer to the riot-api website for parameters:
	 
	 *  @param request: gamename (lol in this case)/spectator, match, etc.
	 *  @param subrequest: summoners, active games etc.
	 *  @param parameter: depends on request and subrequest, can be some id or summonername
	 *  @return JSONObject
	 *  */
	@Override
	public JSONObject readAPI(String request, String subrequest, String parameter) {
		String jsonString = "";
		JSONObject jObject = null;
		try {
			URL url = new URL("https://" + region + ".api.riotgames.com/" + request + "/" 
					+ subrequest + "/" + parameter + "?api_key=" + lolapikey);
			System.out.println(url);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;
			;

			while ((output = br.readLine()) != null) {
				jsonString += output;
				//System.out.println(jsonString);

			}
			jObject = new JSONObject(jsonString);
			//System.out.println(jObject);
			conn.disconnect();
		} catch (Exception e) {
			System.out.println("Exception in readAPI " + e);
		}
		return jObject;
	}
	/**
	 * returns an Arraylist of basic user data from the lol api.
	 * the order of the returned arraylist is:
	 * [accountId, id, name, profileIconId, puuid, revisionDate, summonerlevel]
	 * @param summonerName: string that user uses to login to league of legends
	 * @return List of userdata from specific user
	 * */
 
	@Override
	public List<String> readUserDataByName(String summonerName) {
		JSONObject jObject;
		String request = "lol/summoner/v4";
		String subrequest = "summoners";
		String parameter = "by-name/" + summonerName;
		jObject = readAPI(request, subrequest, parameter);
		Iterator<String> keys = jObject.keys();
		List<String> keysList = new ArrayList<String>();
	      while (keys.hasNext()) {
	        keysList.add(keys.next());
	      }
	      Collections.sort(keysList);
		for (String key : keysList) {
	        userData.add(jObject.get(key).toString());
		}
		return userData;
	}

	/**
	 * returns an arraylist of data from a currently running game
	 * @param encryptedSummonerId: summonerID used in game
	 * @return List of gamedata from ongoing game
	 * */
	@Override
	public List<String> getCurrentGameData(String encryptedSummonerId) {
		JSONObject jObject;
		String request = "lol/spectator/v4";
		String subrequest = "active-games";
		String parameter = "by-summoner/" + encryptedSummonerId;
		jObject = readAPI(request, subrequest, parameter);
		
		
		Iterator<String> keys = jObject.keys();
		List<String> keysList = new ArrayList<String>();
	      while (keys.hasNext()) {
	        keysList.add(keys.next());
	      }
	      Collections.sort(keysList);
		for (String key : keysList) {
	        currentmatchData.add(jObject.get(key).toString());
		}
		
		return currentmatchData;
		
	}

	/**
	 * returns the timeline of the last played match as a JSONObject, works only for matches that have already ended
	 * @param latestMatchId: id of latest match
	 * @return JSONObject of latest matches timeline
	 * */
	@Override
	public JSONObject getLatestMatchTimeline(String latestMatchId) {
		JSONObject latestMatchData;
		String request = "lol/match/v4";
		String subrequest = "timelines/by-match";
		String parameter = latestMatchId;
		latestMatchData = readAPI(request, subrequest, parameter);
		
		
		System.out.println(latestMatchData);
		return latestMatchData;
		
	}

	/**Returns a list of played matches as JSONArray
	 * @param encryptedAccountId: accountId in game
	 * @return JSONArray of played matches
	 * */
	@Override
	public JSONArray getMatchlist(String encryptedAccountId) {
		JSONArray matchList;
		JSONObject jObject;
		String request = "lol/match/v4";
		String subrequest = "matchlists/by-account";
		String parameter = encryptedAccountId;
		jObject = readAPI(request, subrequest, parameter);
		matchList = jObject.getJSONArray("matches");		
		//System.out.println(matchList);
		//System.out.println(jsonArray.getJSONObject(1));
		//System.out.println("TESTING: " + matchList.get(1));
		return matchList;
	}	
	/**
	 * Returns latest played matches ID as a String
	 * @param encryptedAccountId: accountId in game
	 * @return latest matchid as String
	 * */
	public String getLatestMatchId(String encryptedAccountId) {
		String matchId = getMatchlist(encryptedAccountId).getJSONObject(0).get("gameId").toString();
		lastMatchId = matchId;
		return matchId;
	}
	
}

