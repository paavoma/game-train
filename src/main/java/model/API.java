package model;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import view.GametrainController;


/**@author Ninja
 * The Api for reading data from dota api.
 */
public class API {

	/** The guicontrol. */
	GametrainController guicontrol;
	
	/** The player IDs. */
	String dotaplayerID = "480412663", csplayerID;
	
	/** My steamapikey. */
	static String mySteamapikey = "BA8AA3AF84BF8E42D97BF8AE50500121";

	/** The json string. */
	String jsonString = "";

	/**
	 * Instantiates a new api reader.
	 *
	 * @param guicontrol the guicontrol
	 */
	public API(GametrainController guicontrol) {
		this.guicontrol = guicontrol;
	}

	/**
	 * Instantiates a new api.
	 *
	 * @param guicontrol the guicontrol
	 * @param dotaplayerID the dotaplayer ID
	 * @param csplayerID the csplayer ID
	 */
	public API(GametrainController guicontrol, String dotaplayerID, String csplayerID) {
		this.guicontrol = guicontrol;
		this.dotaplayerID = dotaplayerID;
		this.csplayerID = csplayerID;
	}

	/**
	 * Gets data from the API.
	 *
	 * @param request the request
	 * @param subrequest the subrequest
	 * @param parameter the parameter
	 */
	public void readAPI(String request, String subrequest, String parameter) {
		try {
			// example
			// "https://api.steampowered.com/IDOTA2Match_570/GetMatchDetails/v1?key="+mySteamapikey+"&match_id="+matchID)
			URL url = new URL("https://api.steampowered.com/" + request + "/" + subrequest + "?key=" + mySteamapikey
					+ "&" + parameter);
			System.out.println(url);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP Error code : " + conn.getResponseCode());
			}
			InputStreamReader in = new InputStreamReader(conn.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;

			while ((output = br.readLine()) != null) {
				jsonString += output;
				// System.out.println(output);

			}
			System.out.println(jsonString);
			conn.disconnect();
		} catch (Exception e) {
			System.out.println("Exception in API, readMatchInfo:- " + e);
		}

	}

	/**
	 * Read dota match details.
	 *
	 * @param matchID the match ID
	 */
	public void readDotaMatchDetails(String matchID) {
		// https://api.steampowered.com/IDOTA2Match_205790/GetMatchDetails/v1
		String request = "IDOTA2Match_205790";
		String subrequest = "GetMatchDetails/v1";
		String parameter = "match_id=" + matchID;
		readAPI(request, subrequest, parameter);
	}

	/**
	 * Readlatest dota matches.
	 */
	public void readlatestDotaMatches() {
		// https://api.steampowered.com/IDOTA2Match_205790/GetMatchHistory/v1
		String request = "IDOTA2Match_205790";
		String subrequest = "GetMatchHistory/v1";
		String parameter = "account_id=" + dotaplayerID;
		readAPI(request, subrequest, parameter);
	}

	/**
	 * Gets the player match details.
	 *
	 * @param item the item
	 * @return the player match details
	 */
	public String getPlayerMatchDetails(String item) {
		JSONObject jsonObj = new JSONObject(jsonString);

		String stat = "";
		if (item.equals("radiant_win")) {
			stat = getResultMatchDetails(item);
			return stat;
		}
		JSONObject player = (JSONObject) jsonObj.getJSONObject("result").getJSONArray("players").get(3);
		// System.out.println("->"+player.getInt(item));
		try {
			stat = player.getString(item);
			return stat;
		} catch (Exception e) {

		}
		try {
			int statInt = player.getInt(item);
			stat = Integer.toString(statInt);
			return stat;

		} catch (Exception e) {

		}
		try {
			Boolean statBoolean = player.getBoolean(item);
			stat = Boolean.toString(statBoolean);
			return stat;
		} catch (Exception e) {
			System.out.println("stat not String/Int/Boolean?" + e);
		}
		return stat;
	}

	/**
	 * Gets the result match details.
	 *
	 * @param item the item
	 * @return the result match details
	 */
	public String getResultMatchDetails(String item) {
		JSONObject jsonObj = new JSONObject(jsonString);
		String stat = "";
		try {
			stat = jsonObj.getJSONObject("result").getString(item);
			return stat;
		} catch (Exception e) {

		}
		try {
			int statInt = jsonObj.getJSONObject("result").getInt(item);
			stat = Integer.toString(statInt);
			return stat;

		} catch (Exception e) {

		}
		try {
			Boolean statBoolean = jsonObj.getJSONObject("result").getBoolean(item);
			stat = Boolean.toString(statBoolean);
			return stat;
		} catch (Exception e) {
			System.out.println("stat not String/Int/Boolean?" + e);
		}
		return stat;
	}
}
