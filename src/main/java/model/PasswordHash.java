package model;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * PasswordHash class. Inside methods that are used for securing user password.
 * @author Eelis Melto
 */
public class PasswordHash {
	private GameTrain db;
	private static final SecureRandom rand = new SecureRandom();
	private static final int ITERATIONS = 10000;
	private static final int KEY_LENGTH = 256;
	
	/**
	 * PassWordHash class constructor.
	 */
	public PasswordHash() {
		this.db = new GameTrain();
	}

	/**
	 * This method is used for hashing users given password with salt.
	 * @param password password to hash
	 * @param salt salt for the password
	 * @return hashed password - Array of containing bytes.
	 */
	
	public static byte[] hashPassword(char[] password, byte[] salt) {
		PBEKeySpec spec = new PBEKeySpec(password, salt, ITERATIONS, KEY_LENGTH);
		Arrays.fill(password, Character.MIN_VALUE);
		try {
			SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			return skf.generateSecret(spec).getEncoded();
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new AssertionError("Error while hashing a password: " + e.getMessage(), e);
		} finally {
	     spec.clearPassword();
	   }		
	}
	
	/**
	 * This method is used to verify users given password is same as that on database.
	 * @param password password to verify
	 * @param salt salt to accompany with password
	 * @param expectedHash the value that is supposed to be matching
	 * @return boolean - true if its same and false if not.
	 */
	
	public static boolean verifyHashedPassword(char[] password, byte[] salt, byte[] expectedHash) {
	    byte[] hashedpassword = hashPassword(password, salt);
	    Arrays.fill(password, Character.MIN_VALUE);
	    if (hashedpassword.length != expectedHash.length) return false;
	    for (int i = 0; i < hashedpassword.length; i++) {
	      if (hashedpassword[i] != expectedHash[i]) return false;
	    }
	    return true;
	  }
	/**
	 * Method is used to generate salt for new user. 
	 * @return salt - array containing bytes.
	 */
	public byte[] generateUserSalt() {
		byte[] salt = new byte[16];
	    rand.nextBytes(salt);
	    return salt;
	}
	
	/**
	 * Method is used to get users salt.
	 * @param email - String used to get correct users salt.
	 * @return salt - array containing bytes, if user with given email exists and null if user doesn't exist.
	 */
	public byte[] getUserSalt(String email) {
		try {
			User user = db.readUserByEmail(email);
			return user.getSalt();
		} catch (Exception e) {
			System.out.println("Couldn't find user with given email.");
			return null;
		}
		
	}
}
