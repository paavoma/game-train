package model;

import javax.persistence.*;

@Entity
@Table(
		name="GAMES",
			uniqueConstraints = {
					@UniqueConstraint(columnNames = "gameName")
			}
		)
public class Games {
	
	@Id
	@GeneratedValue
	@Column(name="gameID")
	private int gameID;
	
	@Column(name="gameName")
	private String gameName;
	
	/*@OneToMany
	@JoinColumn(name="USERSTATS")
	private UserStats userStats;
	*/
	public Games() {
	}
	
	public Games(int gameID, String gameName) {
		super();
		this.gameID = gameID;
		this.gameName = gameName;
	}

	public int getGameID() {
		return gameID;
	}

	public void setGameID(int gameID) {
		this.gameID = gameID;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	@Override
	public String toString() {
		return "Games [gameID=" + gameID + ", gameName=" + gameName + "]";
	}


}
