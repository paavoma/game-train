package model;
/**
 * @author Petteri Piipponen, Eelis Melto
 * Class for database queries
 * */

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import org.hibernate.Transaction;

public class GameTrain {
	
	private SessionFactory sessionfactory = null;
	
	private final StandardServiceRegistry registry = 
			new StandardServiceRegistryBuilder().configure().build();
	
	/**
	 * Constructor to connect to MySql database.
	 * creates sessionfactory when called if there is a connection to database
	 * Konstruktori joka luo yhteyden MySQL tietokantaan.
	 */
	
	public GameTrain() {
		try {
			sessionfactory = 
					new MetadataSources(registry).buildMetadata().buildSessionFactory();
		}
		catch (Exception e) {
			System.out.println("sessiontehtaan luonti epäonnistui.");
			StandardServiceRegistryBuilder.destroy(registry);
			e.printStackTrace();
			System.exit(-1);
		}
	}
	/**Method that creates user into database
	 * Metodi joka luo käyttäjän tietokantaan
	 * @param user: user object
	 * @return boolean true if user created in db, false if not
	 */
	public boolean createUser(User user) {
		Transaction transaction = null;
		User u = user;
		try(Session session = sessionfactory.openSession()){
			transaction = session.beginTransaction();
			session.saveOrUpdate(u);
			transaction.commit();
			return true;
		}catch(Exception e) {
			if(transaction != null){
				transaction.rollback();
				throw e;
			}
			return false;
		}
	}
	/**
	 * Method that reads users data by userID from database
	 * metodi joka hakee käyttäjän tiedot userID:llä tietokannasta
	 * @param userID, Id for specific user from database
	 * @return user object
	 */
	public User readUserById(int userID) {
		User user = new User();
		try (Session session = sessionfactory.openSession()) {
			session.beginTransaction();
			
			
			session.load(user, userID);
			System.out.println(user.getUsername());
			
			session.getTransaction().commit();
			session.close();
						
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return user;
	}
	/**
	 * Method that reads users data by userID from database
	 * metodi joka hakee käyttäjän tiedot userID:llä tietokannasta
	 * @param userName: users username in database
	 * @return user object
	 */
	public User readUserByName(String userName) {
		int id = 0;
		User user = new User();
		try (Session session = sessionfactory.openSession()) {
			
			session.beginTransaction();
			@SuppressWarnings("unchecked")
			List<User> result = session.createQuery("from User").getResultList();
			for (User u : result) {
				if (u.getUsername().equals(userName)) {
					id = u.getUserID();
				}else
					System.out.println("not this one");
			}
			session.getTransaction().commit();
			session.close();
		try(Session session2 = sessionfactory.openSession()){
			session2.beginTransaction();
					session2.load(user, id);	
					session2.getTransaction().commit();
					session2.close();
		}
		
		}catch (Exception e) {
			e.printStackTrace();
			
			
		}return user;
	/**
	*Reads user from database
	*@param userEmail: users email specified in database
	*@return user as object
	*/
	}
	public User readUserByEmail(String userEmail) {
		int id = 0;
		User user = new User();
		try (Session session = sessionfactory.openSession()) {
			
			session.beginTransaction();
			@SuppressWarnings("unchecked")
			List<User> result = session.createQuery("from User").getResultList();
			for (User u : result) {
				if (u.getEmail().equals(userEmail)) {
					id = u.getUserID();
				}else
					System.out.println("not this one");
			}
			session.getTransaction().commit();
			session.close();
		try(Session session2 = sessionfactory.openSession()){
			session2.beginTransaction();
					session2.load(user, id);	
					session2.getTransaction().commit();
					session2.close();
		}
		
		}catch (Exception e) {
			e.printStackTrace();
			
			
		}return user;
		
	}
	
	/**
	 * updates user in database
	 * Metodi päivittää tietokannassa olevaa käyttäjää.
	 * @param user: user object
	 * @return boolean true if update succesful and false if unable to update or nothing to update
	 */
	public boolean updateUser(User user) {
		
		try (Session session = sessionfactory.openSession()) {
			session.beginTransaction();
			User u = (User)session.get(User.class, user.getUserID());
			if (u != null) {
				u.setUsername(u.getUsername());
				u.setEmail(u.getEmail());
				u.setPassword(u.getPassword());
				u.setProfpicture(u.getProfpicture());
			} else {
				System.out.println("Nothing to update.");
				return false;
			}
			session.getTransaction().commit();
			session.close();
		}
		return true;
	}
	
	/**
	 * Deletes user from database by userID
	 * Poistaa tietokannasta käyttäjän jonka userID on metodiin annettava parametri.
	 * @param userID: userID of certain user in db
	 * @return boolean, true if user deleted and false if user not deleted
	 */
	public boolean deleteUser(int userID) {
		try (Session session = sessionfactory.openSession()) {
			session.beginTransaction();
			User u = (User)session.get(User.class, userID);
			if (u != null) {
				session.delete(u);
			} else {
				System.out.println("Käyttäjän poistaminen ei onnistunut.");
				return false;
			}
			session.getTransaction().commit();
			session.close();
		}
		return true;
	}	
	
	/**
	 * outprints all users from database
	 * Tulostaa kaikki käyttäjät tietokannasta.
	 * @return array of all users in datebase
	 */
	public User[] readUsers() {
		try (Session session = sessionfactory.openSession()) {
			session.beginTransaction();
			@SuppressWarnings("unchecked")
			List<User> result = session.createQuery("from user").getResultList();
			for (User user : result) {
				System.out.println("user ("+ user.getUserID() +") : "+user.getUsername()+" :"+user.getEmail()+" :"+user.getPassword()+" :"+user.getProfpicture()+" ");
			}
			
			session.getTransaction().commit();
			session.close();
			
			User[] returnlist = new User[result.size()];
			
			return (User[])result.toArray(returnlist);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			
		}
		
		
		
	}
	/**Method that creates game into database
	 * Metodi joka luo pelin tietokantaan
	 * @param games: Games object
	 * @return boolean true if new game added to db, false if not
	 */
	public boolean createGames(Games games) {
		Transaction transaction = null;
		Games g = games;
		try(Session session = sessionfactory.openSession()){
			transaction = session.beginTransaction();
			session.saveOrUpdate(g);
			transaction.commit();
			return true;
		}catch(Exception e) {
			if(transaction != null){
				transaction.rollback();
				throw e;
			}
			return false;
		}
	}
	/**
	 * Method that reads games data by gameID from database
	 * metodi joka hakee pelin tiedot gameID:llä tietokannasta
	 * @param gameID: gameID specified in database for particular game
	 * @return games object with parameters added by database
	 */
	public Games readGames(int gameID) {
		Games games = new Games();
		try (Session session = sessionfactory.openSession()) {
			session.beginTransaction();
			
			
			session.load(games, gameID);
			System.out.println(games.getGameName());
			
			session.getTransaction().commit();
			session.close();
						
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return games;
	}
	/**
	 * Method that reads games data by gameName from database
	 * metodi joka hakee pelin tiedot gameName:lla tietokannasta
	 * @param gameName: games name in database
	 * @return games object with parameters added by database
	 */
	
	public Games readGameByName(String gameName) {
		int id = 0;
		Games game = new Games();
		try (Session session = sessionfactory.openSession()) {
			
			session.beginTransaction();
			@SuppressWarnings("unchecked")
			List<Games> result = session.createQuery("from Games").getResultList();
			for (Games g : result) {
				if (g.getGameName().equals(gameName)) {
					id = g.getGameID();
				}else
					System.out.println("not this one");
			}
			session.getTransaction().commit();
			session.close();
		try(Session session2 = sessionfactory.openSession()){
			session2.beginTransaction();
					session2.load(game, id);	
					session2.getTransaction().commit();
					session2.close();
		}
		
		}catch (Exception e) {
			e.printStackTrace();
			
			
		}return game;
		
	}
	
	/**
	 * updates game in database
	 * Metodi päivittää tietokannassa olevaa peliä.
	 * @param games: games object
	 * @return boolean true if game updated in db and false if not
	 */
	public boolean updateGames(Games games) {
		
		try (Session session = sessionfactory.openSession()) {
			session.beginTransaction();
			Games g = (Games)session.get(Games.class, games.getGameID());
			if (g != null) {
				g.setGameName(g.getGameName());
			} else {
				System.out.println("Nothing to update.");
				return false;
			}
			session.getTransaction().commit();
			session.close();
		}
		return true;
	}
	
	/**
	 * Deletes game from database by gameID
	 * Poistaa tietokannasta käyttäjän jonka gameID on metodiin annettava parametri.
	 * @param gameID: gameID specified in database
	 * @return true if game deleted from db and false if not
	 */
	public boolean deleteGames(int gameID) {
		try (Session session = sessionfactory.openSession()) {
			session.beginTransaction();
			Games g = (Games)session.get(Games.class, gameID);
			if (g != null) {
				session.delete(g);
			} else {
				System.out.println("Käyttäjän poistaminen ei onnistunut.");
				return false;
			}
			session.getTransaction().commit();
			session.close();
		}
		return true;
	}	
	
	/**
	 * outprints all games from database
	 * Tulostaa kaikki pelit tietokannasta.
	 * @return array of games from db
	 */
	public Games[] readAllGames() {
		try (Session session = sessionfactory.openSession()) {
			session.beginTransaction();
			@SuppressWarnings("unchecked")
			List<Games> result = session.createQuery("from games").getResultList();
			for (Games games : result) {
				System.out.println("user ("+ games.getGameID() +") : "+games.getGameName()+" ");
			}
			
			session.getTransaction().commit();
			session.close();
			
			Games[] returnlist = new Games[result.size()];
			
			return (Games[])result.toArray(returnlist);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			
		}
		
		
		
	}
	/**Method that creates userstats into database
	 * Metodi joka luo statsit tietokantaan
	 * @param userstats: userstats object
	 * @return boolean true if new userstats created in db, false if not
	 */
	public boolean createUserstat(UserStats userstats) {
		Transaction transaction = null;
		UserStats u = userstats;
		try(Session session = sessionfactory.openSession()){
			transaction = session.beginTransaction();
			session.saveOrUpdate(u);
			transaction.commit();
			return true;
		}catch(Exception e) {
			if(transaction != null){
				transaction.rollback();
				throw e;
			}
			return false;
		}
	}
	/**
	 * Method that reads userstat data by userstatID from database
	 * metodi joka hakee statistiikkojen tiedot userstatID:llä tietokannasta
	 * @param userstatID: userstatID specified for certain stat in database
	 * @return userstats object
	 */
	public UserStats readUserstats(int userstatID) {
		UserStats userstats = new UserStats();
		try (Session session = sessionfactory.openSession()) {
			session.beginTransaction();
			
			
			session.load(userstats, userstatID);
			System.out.println(userstats.getUserStatsID());
			
			session.getTransaction().commit();
			session.close();
						
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return userstats;
	}
	
	/**
	 * updates userstats in database
	 * Metodi päivittää tietokannassa olevia statistiikoja.
	 * @param userstats: userstats object
	 * @return boolean true if stats updated, false if not
	 */
	public boolean updateUserstats(UserStats userstats) {
		int id = userstats.getUserStatsID();
		try (Session session = sessionfactory.openSession()) {
			session.beginTransaction();
			System.out.println("Modelin userstats id"+id);
			UserStats u = (UserStats) session.get(UserStats.class, new Integer(id));
			if (u != null) {
				u.setDate(userstats.getDate());
				u.setTime(userstats.getTime());
				u.setComments(userstats.getComments());
				u.setDeaths(userstats.getDeaths());
				u.setGametime(userstats.getGametime());
				u.setKills(userstats.getKills());
				u.setUserStatsID(userstats.getUserStatsID());
				u.setWin(userstats.getWin());
				u.setGsession(userstats.getGsession());
				 

			} else {
				System.out.println("Nothing to update.");
				return false;
			}
			session.getTransaction().commit();
			session.close();
		}
		return true;
	}
	
	/**
	 * Deletes userstats from database by userstatID
	 * Poistaa tietokannasta statistiikkoja jonka userstatID on metodiin annettava parametri.
	 * @param  userstatID usertatID specified in db
	 * @return true if stats deleted from db, false if not
	 */
	public boolean deleteUserstats(int userstatID) {
		try (Session session = sessionfactory.openSession()) {
			session.beginTransaction();
			UserStats u = (UserStats)session.get(UserStats.class, userstatID);
			if (u != null) {
				session.delete(u);
			} else {
				System.out.println("Käyttäjän poistaminen ei onnistunut.");
				return false;
			}
			session.getTransaction().commit();
			session.close();
		}
		return true;
	}	
	/**
	 * Deletes all userstats with specific sessionId
	 * @param sessionId sessionId specified in database
	 * @return true if deleting stats worked, false if not
	 * */
	public boolean deleteUserStatsWithSessionID(int sessionId) {
		try (Session session = sessionfactory.openSession()) {
			session.beginTransaction();
			Query<?> q = session.createQuery("delete from UserStats where session=:s");
			q.setParameter("s", sessionId);
			int status = q.executeUpdate();  
			session.close();
			System.out.println(status + "stats deleted");
			return true;
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
			
		}
	}
	
	/**
	 * outprints all userstats from database
	 * Tulostaa kaikki statistiikat tietokannasta.
	 * @return array of al userstats
	 */
	public UserStats[] readAllUserStats() {
		try (Session session = sessionfactory.openSession()) {
			session.beginTransaction();
			@SuppressWarnings("unchecked")
			List<UserStats> result = session.createQuery("from userstats").getResultList();
			for (UserStats userstats : result) {
				System.out.println("userstats ("+ userstats.getUserStatsID() +") : "+ userstats.getDate() +
						") : "+ userstats.getTime() +" :"+ userstats.getComments() +
						" :"+ userstats.getDeaths() +" :"+ userstats.getGametime() +" :"+ userstats.getKills() +
						" :"+  userstats.getWin() +" :"+ userstats.getGsession() + " ");
			}
			
			session.getTransaction().commit();
			session.close();
			
			UserStats[] returnlist = new UserStats[result.size()];
			
			return (UserStats[])result.toArray(returnlist);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			
		}
		
		
		
	}
	/**
	 * This method is used to return all userstats with sessionId.
	 * @param sessionId: sessionId specified in database
	 * @return all userstats with certain sessionId
	 */
	public UserStats[] readUsersUserStatsWithSessionId(int sessionId) {
		try (Session tSession = sessionfactory.openSession()) {
			tSession.beginTransaction();
			@SuppressWarnings("unchecked")
			List<UserStats> result = tSession.createQuery("from UserStats where session="+sessionId).getResultList();
			
			tSession.getTransaction().commit();
			tSession.close();
			
			UserStats[] returnlist = new UserStats[result.size()];
			
			return (UserStats[])result.toArray(returnlist);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			
		}
	}
	/**
	 * Method that reads latest gamesessions sessionID and returns it.
	 * @param userID: userID specified in db
	 * @param gameID: gameID specified in db
	 * @return int sessionID of latest gamesession
	 */
	public int readLatestGameSession(int userID, int gameID) {
		try (Session session = sessionfactory.openSession()) {
			session.beginTransaction();
			String hql = "Select MAX(sessionID) FROM GSession where USER=:A and GAMES=:B"; 
			Query<Integer> query = session.createQuery(hql);
			query.setParameter("A", userID);
			query.setParameter("B", gameID);
			
			int result = query.getSingleResult();
			session.getTransaction().commit();
			session.close();
			
			return result;
			
		} catch (Exception e) {
			System.out.println("-------------------------------------------");
			e.printStackTrace();
			return 0;
			
		}
	}
	
	/**
	 * NOT IN USE
	 * @param sessionID not in use
	 * @return not in use
	 */
	public int readUserStatsID(int sessionID) {
		try (Session session = sessionfactory.openSession()) {
			session.beginTransaction();
			String hql = "Select MIN(userStatsID) FROM UserStats WHERE session=:A";
			Query<Integer> query = session.createQuery(hql);
			query.setParameter("A", sessionID);
			int result = query.getSingleResult();
			
			session.getTransaction().commit();
			session.close();
			
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}
	
	

	/**Method that reates gsession into database
	 * Metodi joka luo pelisession tietokantaan
	 * @param gsession: GSession object
	 * @return true if gamesession created in db, false if not
	 */
	public boolean createGameSession(GSession gsession) {
		Transaction transaction = null;
		GSession u = gsession;
		try(Session tSession = sessionfactory.openSession()){
			transaction = tSession.beginTransaction();
			tSession.saveOrUpdate(u);
			transaction.commit();
			return true;
		}catch(Exception e) {
			if(transaction != null){
				transaction.rollback();
				throw e;
			}
			return false;
		}
	}
	/**
	 * Method that reads gsession data by sessionID from database
	 * metodi joka hakee sessioiden tiedot sessionID:llä tietokannasta
	 * @param sessionID: sessionID specified in db
	 * @return GSession object
	 */
	public GSession readGameSession(int sessionID) {
		GSession gsession = new GSession();
		try (Session tSession = sessionfactory.openSession()) {
			tSession.beginTransaction();
			
			
			tSession.load(gsession, sessionID);
			System.out.println(gsession.getSessionID());
			
			tSession.getTransaction().commit();
			tSession.close();
						
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		return gsession;
	}
	/**
	 * updates gsession in database
	 * Metodi päivittää tietokannassa olevia statistiikoja.
	 * @param gsession: GSession object
	 * @return true if gamesession updated in db, false if not
	 */
	public boolean updateGameSession(GSession gsession) {
		
		try (Session tSession = sessionfactory.openSession()) {
			tSession.beginTransaction();
			GSession u = (GSession)tSession.get(GSession.class, gsession.getSessionID());
			if (u != null) {
				u.setSessionID(u.getSessionID());
				u.setSessionName(u.getSessionName());
				u.setStartTime(u.getStartTime());
				u.setEndTime(u.getEndTime());
				u.setUser(u.getUser());
				u.setGames(u.getGames());

			} else {
				System.out.println("Nothing to update.");
				return false;
			}
			tSession.getTransaction().commit();
			tSession.close();
		}
		return true;
	}
	/**Method for reading all users sessions of a specific game
	 * @param userId userID specified in db
	 * @param gameId gameID specified in db
	 * @return array of GSessions
	 * */
	public GSession[] readUsersSessions(int userId, int gameId) {
		try (Session tSession = sessionfactory.openSession()) {
			tSession.beginTransaction();
			@SuppressWarnings("unchecked")
			List<GSession> result = tSession.createQuery("from GSession where USER="+userId+" and GAMES="+gameId).getResultList();
			for (GSession gsession : result) {
				System.out.println("session ("+ gsession.getSessionID() +") : " +gsession.getSessionName()+" :"+gsession.getGames()+" :"+gsession.getUser()+" :" + gsession.getStartTime()+" :"+ gsession.getEndTime()+" ");
			}
			
			tSession.getTransaction().commit();
			tSession.close();
			
			GSession[] returnlist = new GSession[result.size()];
			
			return (GSession[])result.toArray(returnlist);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			
		}
	}
	/**
	 * Deletes gsession from database by sessionID
	 * Poistaa tietokannasta statistiikkoja jonka sessionID on metodiin annettava parametri.
	 * @param sessionID: sessionid specified in db
	 * @return boolean true if session deleted from db, false if not
	 */
	public boolean deleteSession(int sessionID) {
		try (Session tSession = sessionfactory.openSession()) {
			tSession.beginTransaction();
			String hql = "Delete FROM GSession WHERE sessionID=:A";
			Query<?> query = tSession.createQuery(hql);
			query.setParameter("A", sessionID);
			query.executeUpdate();
			
			tSession.getTransaction().commit();
			tSession.close();
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}	
	/**
	 * outprints all gsession from database
	 * Tulostaa kaikki statistiikat tietokannasta.
	 * @return array of all gamesessions in db
	 */
	public GSession[] readAllGSession() {
		try (Session tSession = sessionfactory.openSession()) {
			tSession.beginTransaction();
			@SuppressWarnings("unchecked")
			List<GSession> result = tSession.createQuery("from GAMESESSION").getResultList();
			for (GSession gsession : result) {
				System.out.println("gsession ("+ gsession.getSessionID() +") : "+ gsession.getSessionID() +" :"+gsession.getSessionName()+":"+gsession.getGames()+" :"+gsession.getUser()+" :" + gsession.getStartTime()+" :"+ gsession.getEndTime()+" ");
			}
			
			tSession.getTransaction().commit();
			tSession.close();
			
			GSession[] returnlist = new GSession[result.size()];
			
			return (GSession[])result.toArray(returnlist);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
			
		}
		
		
		
	}
}

