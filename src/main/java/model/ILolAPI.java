package model;

/**
 * @author Petteri Piipponen
 * Interface used by class LolAPI
 * */

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public interface ILolAPI {
	
	public JSONObject readAPI(String request, String subrequest, String parameter);
	public List<String> readUserDataByName(String summonerName);
	public List<String> getCurrentGameData(String encryptedSummonerId);
	public JSONObject getLatestMatchTimeline(String encryptedAccountId);
	public JSONArray getMatchlist(String encryptedAccountId);
	public String getLatestMatchId(String encryptedAccountId);
	
	
}

