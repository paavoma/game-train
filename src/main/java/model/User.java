package model;

import javax.persistence.*;

@Entity
@Table(
		name="USER",
		uniqueConstraints = {
				@UniqueConstraint(columnNames = "email")
}
)
public class User {
	
	@Id
	@GeneratedValue
	@Column(name="userID")
	private int userID;
	
	@Column(name="username")
	private String username;
	
	@Column(name="password")
	private byte[] password;
	
	@Column(name="salt")
	private byte[] salt;
	
	@Column(name="email")
	private String email;
	
	@Column(name="profpicture")
	private String profpicture;
	
	
	public User() {
	}
	
	public User(int userID, String username, String password, String email) {
		super();
		this.userID = userID;
		this.username = username;
		this.password = password.getBytes();
		this.email = email;
		//this.profpicture = profpicture;
	}


	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public byte[] getPassword() {
		return password;
	}

	public void setPassword(byte[] password) {
		this.password = password;
	}

	public byte[] getSalt() {
		return salt;
	}

	public void setSalt(byte[] salt) {
		this.salt = salt;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getProfpicture() {
		return profpicture;
	}

	public void setProfpicture(String profpicture) {
		this.profpicture = profpicture;
	}

	@Override
	public String toString() {
		return "User [userID=" + userID + ", username=" + username + ", password=" + password + ", salt=" + salt
				+ ", email=" + email + ", profpicture=" + profpicture + "]";
	}
	
}
