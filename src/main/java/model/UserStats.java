package model;

import java.sql.Time;
import java.util.Date;

import javax.persistence.*;
/**
 * This class is UserStats and it's used to make a table USERSTATS to database.
 * Class allows to store users statistics to UserStats variables, with it's constructor 
 * or with its set Methods.
 * @author Eelis Melto, Petteri Piipponen
 *
 */
@Entity
@Table(name="USERSTATS")
public class UserStats {
	
	@Id
	@GeneratedValue
	@Column(name="userStatsID")
	private int userStatsID;
	
	@Column(name="kills")
	private int kills;
	
	@Column(name="deaths")
	private int deaths;
	
	@Column(name="win")
	private int win;
	
	/** The stat1 in CSGO this is won rounds.*/
	@Column(name="stat1")
	private int stat1; //CSGO won rounds
	/** The stat2 in CSGO this is enemy rounds. */
	@Column(name="stat2")
	private int stat2; //CSGO enemy rounds
	/** The stat3 in CSGO this is players mvps */
	@Column(name="stat3")
	private int stat3; //CSGO mvps
	/** The stat4 in CSGO this is players damage done */
	@Column(name="stat4")
	private int stat4; //CSGO damage done
	/** The stat5 in CSGO this is players money spent */
	@Column(name="stat5")
	private int stat5; //CSGO money spent
	/** The stat6 in CSGO match score */
	@Column(name="stat6")
	private int stat6; //CSGO match score
	
	@Column(name="comments")
	private String comments;
	

	@Column(name = "gameTime")
	private Time gametime;

	
	@Temporal(TemporalType.DATE)
	@Column(name = "date")
    private Date date;
    

	@Column(name = "time")
    private Time time;

	
	@ManyToOne
	@JoinColumn(name="session")
	private GSession gsession;
	
	
	
	public UserStats() {
	}

	public UserStats(int userStatsID, Time gametime, int kills, int deaths, int win,int stat1,int stat2,int stat3,int stat4,int stat5,int stat6,String comments,Date date, Time time, GSession gsession) {
		super();
		this.userStatsID = userStatsID;
		this.gametime = gametime;
		this.kills = kills;
		this.deaths = deaths;
		this.win = win;
		this.stat1 = stat1;
		this.stat2 = stat2;
		this.stat3 = stat3;
		this.stat4 = stat4;
		this.stat5 = stat5;
		this.stat6 = stat6;
		this.comments = comments;
		this.date = date;
		this.time = time;
		this.gsession = gsession;
	}

	public int getUserStatsID() {
		return userStatsID;
	}

	public void setUserStatsID(int userStatsID) {
		this.userStatsID = userStatsID;
	}


	public Time getGametime() {
		return gametime;
	}

	public void setGametime(Time gametime) {
		this.gametime = gametime;
	}

	public int getKills() {
		return kills;
	}

	public void setKills(int kills) {
		this.kills = kills;
	}

	public int getDeaths() {
		return deaths;
	}

	public void setDeaths(int deaths) {
		this.deaths = deaths;
	}

	public int getWin() {
		return win;
	}

	public void setWin(int win) {
		this.win = win;
	}
	
	public int getStat1() {
		return stat1;
	}
	
	public void setStat1(int stat1) {
		this.stat1 = stat1;
	}
	
	public int getStat2() {
		return stat2;
	}
	
	public void setStat2(int stat2) {
		this.stat2 = stat2;
	}
	
	public int getStat3() {
		return stat3;
	}
	
	public void setStat3(int stat3) {
		this.stat3 = stat3;
	}
	
	public int getStat4() {
		return stat4;
	}
	
	public void setStat4(int stat4) {
		this.stat4 = stat4;
	}
	
	public int getStat5() {
		return stat5;
	}
	
	public void setStat5(int stat5) {
		this.stat5 = stat5;
	}
	
	public int getStat6() {
		return stat6;
	}
	
	public void setStat6(int stat6) {
		this.stat6 = stat6;
	}
	
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}


	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public GSession getGsession() {
		return gsession;
	}

	public void setGsession(GSession gsession) {
		this.gsession = gsession;
	}

	@Override
	public String toString() {
		return "UserStats [userStatsID=" + userStatsID + ", kills=" + kills + ", deaths=" + deaths + ", win=" + win
				+", stat1="+stat1+", stat2="+stat2+", stat3="+stat3+", stat4="+stat4+", stat5="+stat5+", stat6="+stat6+ ", comments=" + comments + ", gametime="
				+ gametime + ", date=" + date + ", time=" + time + "]";
	}

	
	
	
}
