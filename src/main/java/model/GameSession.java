package model;

import java.util.ArrayList;
import java.util.Arrays;


/**@author Ninja
 * The Class GameSession.
 */
public class GameSession implements IGameSession {
	
	/** The gametitle. */
	private String gametitle;
	
	/** The ID count. */
	private static int count = 0;
	
	/** The session ID. */
	private int sessionID = 0;
	
	/** The game ID. */
	private int gameID;
	
	/** The database session ID. */
	private int databaseSessionID = 0;

	/** The name for stats in api. */
	private ArrayList<String> trackedstats = new ArrayList<>(); // Api name for dota
	
	/** The stats. */
	private ArrayList<String> stats = new ArrayList<>(); // Stat Data
	
	/** The notes. */
	private ArrayList<String> notes = new ArrayList<>(); // Notes as Strings

	/**
	 * Instantiates a new gamesession.
	 *
	 * @param GameID the game ID
	 */
	public GameSession(int GameID) {
		setGameID(GameID);
		sessionID = count++;
		System.out.println("new session made " + sessionID);

	}

	/**
	 * Gets the game ID.
	 *
	 * @return the game ID
	 */
	public int getGameID() {
		return gameID;
	}

	/**
	 * Sets the game ID.
	 *
	 * @param gameID the new game ID
	 */
	public void setGameID(int gameID) {
		this.gameID = gameID;
	}

	/* 
	 * Gets the gametitle
	 */
	public String getGametitle() {
		return gametitle;
	}

	/* 
	 * Sets the gametitle
	 */
	public void setGametitle(String gametitle) {
		this.gametitle = gametitle;
	}

	/* 
	 * Gets the stats
	 */
	public ArrayList<String> getStats() {
		return stats;
	}

	/* 
	 * Sets the stats
	 */
	public void setStats(ArrayList<String> stats) {
		this.stats = stats;
	}

	/* 
	 * Adds a Note to the Notes arraylist
	 */
	public void addNote(String note) {
		this.notes.add(note);
	}

	/* Sets all notes 
	 */
	public void setNotes(ArrayList<String> notes) {
		this.notes = notes;
	}

	/* 
	* gets the notesee
	 */
	public ArrayList<String> getNotes() {
		return notes;
	}

	/* 
	 * Gets the sessionID
	 */
	public int getSessionID() {
		return sessionID;
	}
	
	/**
	 * Sets the session ID.
	 *
	 * @param sessionID the new session ID
	 */
	public void setSessionID(int sessionID) {
		this.sessionID = sessionID;
	}
	
	/**
	 * Gets the database session ID.
	 *
	 * @return the database session ID
	 */
	public int getDatabaseSessionID() {
		return databaseSessionID;
	}
	
	/**
	 * Sets the database session ID.
	 *
	 * @param databaseSessionID the new database session ID
	 */
	public void setDatabaseSessionID(int databaseSessionID) {
		this.databaseSessionID = databaseSessionID;
	}
}
